import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';

import { NotificacoesComponent } from './components/notificacoes/notificacoes.component';
import { Perfil } from './components/Perfil/perfil.component';
import { EditarPerfil } from './components/EditarPerfil/editarperfil.component';
import { UserService } from './components/shared/user.service';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { ComfirmarEmailComponent } from './components/confirmaremail/confirmaremail.component'
import { InserirAposta } from './components/inserirAposta/inserirAposta.component';
import { VisualizarAposta } from './components/visualizarAposta/visualizarAposta.component';

import { Injectable } from '@angular/core';

import { Home_adminComponent } from './components/admin/home_admin.component';
import { Inserir_desportoComponent } from './components/admin/inserir_desporto/inserir_desporto.component';
import { DenunciasComponent } from './components/denuncias/denuncias.component';
import { Detalhes_denunciaComponent } from './components/denuncias/detalhes_denuncia/detalhes_denuncia.component';
import { Menu_adminComponent } from './components/admin/menu_admin/menu_admin.component';
import { Listar_ligasComponent } from './components/admin/inserir_desporto/listar_ligas/listar_ligas.component';
import { ModalidadeComponent } from './components/admin/modalidade/modalidade.component';
import { Listar_modalidadesComponent } from './components/admin/modalidade/listar_modalidades/listar_modalidades.component';
import { Editar_ligasComponent } from './components/admin/inserir_desporto/listar_ligas/editar_ligas/editar_ligas.component';
import { Lista_usersComponent } from './components/admin/lista_users/lista_users.component';
import { Lista_apostasComponent } from './components/admin/lista_apostas/lista_apostas.component';



@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        NotificacoesComponent,
        HomeComponent,
        Perfil,
        EditarPerfil,
        LoginComponent,
        RegisterComponent,
        InserirAposta,
        VisualizarAposta,
        Lista_usersComponent,
        ResetPasswordComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'notificacoes', component: NotificacoesComponent},
            { path: 'perfil', component: Perfil },
            { path: 'editarperfil', component: EditarPerfil },
            { path: 'reset', component: ResetPasswordComponent },
            { path: 'register', component: RegisterComponent },
            { path: 'inserirAposta', component: InserirAposta },
            { path: 'visualizarAposta', component: VisualizarAposta },
            { path: 'login', component: LoginComponent },
            { path: 'inserirAposta', component: InserirAposta },
            { path: 'visualizarAposta', component: VisualizarAposta },
            { path: 'admin', component: Lista_usersComponent },
            { path: '**', redirectTo: 'home' }
            
        ])
    ]
})
export class AppModuleShared {
}
