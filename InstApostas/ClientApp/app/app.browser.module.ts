import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppModuleShared } from './app.shared.module';
import { AppComponent } from './components/app/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Injectable } from '@angular/core';

import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from "angular5-social-login";
import { UserService } from './components/shared/user.service';


@NgModule({
    imports: [
        BrowserModule,
        AppModuleShared,
        SocialLoginModule
    ],
    providers: [{ provide: AuthServiceConfig, useFactory: getAuthServiceConfigs }, UserService,
    { provide: 'BASE_URL', useFactory: getBaseUrl }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
export function getAuthServiceConfigs() {
    let config = new AuthServiceConfig(
        [
            {
                id: FacebookLoginProvider.PROVIDER_ID,
                provider: new FacebookLoginProvider("178141009560447")
            },
            {
                id: GoogleLoginProvider.PROVIDER_ID,
                provider: new GoogleLoginProvider("966564432540-41pjaj1n8hq8qufs59ht6quvgmpogjcp.apps.googleusercontent.com")
            },
        ]);
    return config;
}
export function getBaseUrl() {
    return document.getElementsByTagName('base')[0].href;
}
