import { Component, Inject } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent {

    public userLogado: Utilizador;

    constructor(private route: ActivatedRoute, private router: Router, private http: Http, @Inject('BASE_URL') private baseUrl: string) {
        http.get(baseUrl + 'api/User/CurrentUser').subscribe(result => {
            this.userLogado = result.json() as Utilizador;
        }, error => console.error(error));

        

    }

    ngOnInit() {

        this.http.get(this.baseUrl + 'api/User/CurrentUser').subscribe(result => {
            this.userLogado = result.json() as Utilizador;
        }, error => console.error(error));

    }
}

interface Utilizador {
    id: number;
}