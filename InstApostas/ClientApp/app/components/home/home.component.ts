import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';


@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {
    public ListaApostas: ApostaHomeInfo[];

    
    constructor(http: Http,  @Inject('BASE_URL') baseUrl: string) {


        http.get(baseUrl + 'api/Apostas/GetApostasInfo').subscribe(result => {
            this.ListaApostas = result.json() as ApostaHomeInfo[];
            console.log(this.ListaApostas);

        }, error => console.error(error));
    }

}


interface ApostaHomeInfo {
    nome: string;
    foto: string;
}