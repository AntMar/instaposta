﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';

@Component({
    selector: 'app-notificacoes',
    templateUrl: './notificacoes.component.html',
    styleUrls: ['./notificacoes.component.css']
})
/** Notificacoes component*/
export class NotificacoesComponent {
    /** Notificacoes ctor */
    public notificacoes: NotificacoesComponent[];

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/Notificacoes/Notifica').subscribe(result => {
            this.notificacoes = result.json() as NotificacoesComponent[];
        }, error => console.error(error));
    }
    public notifImageUrl1 = require("./Images/thumbs-up.png");
    public notifImageUrl2 = require("./Images/comment.png");
    public notifImageUrl3 = require("./Images/Users-Add.png");
}

interface NotificacacoesComponent {
    sourceUser: string;
    url: string;
    tipo: string;
}