﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { NotificacoesComponent } from './notificacoes.component';

let component: NotificacoesComponent;
let fixture: ComponentFixture<NotificacoesComponent>;

describe('Notificacoes component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ NotificacoesComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(NotificacoesComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});