﻿import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, Params } from '@angular/router';
import { FormControl } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { OnInit } from '@angular/core';
import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider
} from 'angular5-social-login';
import { User } from '../shared/user.model';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [UserService, AuthService]
})
/** login component*/
export class LoginComponent implements OnInit {
    ngOnInit(): void {
        throw new Error("Method not implemented.");
    }
    /** login ctor */
    /** login ctor */

    loginForm: FormGroup;
    successMessage: string = '';
    errorMessage: string = '';


    constructor(private fb: FormBuilder, private router: Router, public userService: UserService, public socialAuthService: AuthService) {

        this.createForm();
    }

    createForm() {
        this.loginForm = this.fb.group({
            email: ['', Validators.email],
            password: ['', Validators.required]
        });
    }

    tryLogin(value: any) {

        if (this.loginForm.valid) {
            this.userService.LoginUser(value)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.errorMessage = "";
                            this.successMessage = "Login efetuado com sucesso!!!";
                            this.router.navigate(['/home']);
                            break;
                        }
                        case "erro": {
                            this.successMessage = "";
                            this.errorMessage = "O email introduzido não existe!!!";
                            break;
                        }
                        case "bloqueado": {
                            this.successMessage = "";
                            this.errorMessage = "A sua conta encontra-se bloqueada, por favor contacte o administrador: InstApostasMp5@gmail.com!!!";
                            break;
                        }
                        case "pass": {
                            this.errorMessage = "Password incorreta!!!";
                            this.successMessage = "";
                            break;
                        }
                        case "admin": {
                            this.errorMessage = "";
                            this.successMessage = "Login efetuado com sucesso!!!";
                            this.router.navigate(['/admin']);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                });
        }
        else {
            if (value.email == "" || value.password == "") {
                this.successMessage = "";
                this.errorMessage = "Campos de preenchimento obrigatório!!!";
            }
            else {
                this.successMessage = "";
                this.errorMessage = "Insira um email válido!!!";
            }

        }
    }
    trySocialLogin(socialPlatform: string) {
        let socialPlatformProvider;
        if (socialPlatform == "facebook") {
            socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        } else if (socialPlatform == "google") {
            socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }
        if (socialPlatformProvider != null)
            this.socialAuthService.signIn(socialPlatformProvider).then(
                (userData) => {
                    console.log(socialPlatform + " sign in data : ", userData);

                    this.userService.SocialLogin(userData.email, userData.image, userData.name)
                        .subscribe((data: any) => {
                            switch (data._body) {
                                case "sucesso": {
                                    this.errorMessage = "";
                                    this.successMessage = "Login efetuado com sucesso!!!";
                                    this.router.navigate(['/home']);
                                    break;
                                }
                                case "erro": {
                                    this.successMessage = "";
                                    this.errorMessage = "Por favor efetue o registo!!!";
                                    break;
                                }
                                case "bloqueado": {
                                    this.successMessage = "";
                                    this.errorMessage = "A sua conta encontra-se bloqueada, por favor contacte o administrador: InstApostasMp5@gmail.com!!!";
                                    break;
                                }
                                case "registado": {
                                    this.errorMessage = "Já foi registada uma conta com o seu email, para fazer login terá de inroduzir o email e a password nos campos em cima!!!";
                                    this.successMessage = "";
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }
                           
                        });
                }
            );

    }
}