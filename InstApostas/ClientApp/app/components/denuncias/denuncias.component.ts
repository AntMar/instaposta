﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Text } from '@angular/compiler';

@Component({
    selector: 'app-denuncias',
    templateUrl: './denuncias.component.html',
    styleUrls: ['./denuncias.component.css']
})
/** denuncias component*/
export class DenunciasComponent {
    /** denuncias ctor */


    public denuncias: Denuncia[];

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {

        http.get(baseUrl + 'api/Admin/getDenuncia').subscribe(result => {
            this.denuncias = result.json() as Denuncia[];
            console.log(this.denuncias)
        }, error => console.error(error));
    }
}

interface Denuncia {
    Id_denuncia: number,
    Id_comentario: number | null,
    Id_aposta: number | null,
    Id_denunciante: string,
    Id_denunciado: string,
    Tipo_Denuncia: number,
    data: Date,
    motivo: string,
    Data_Resposta: Date | null,
    resposta_administrador: string | null,
    estado: boolean,
    tipo: string
}