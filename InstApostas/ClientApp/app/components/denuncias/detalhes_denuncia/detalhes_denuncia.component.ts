﻿import { Component, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-detalhes_denuncia',
    templateUrl: './detalhes_denuncia.component.html',
    styleUrls: ['./detalhes_denuncia.component.css']
})
/** detalhes_denuncia component*/
export class Detalhes_denunciaComponent {

    public id_denuncia: number;
    public resposta: string;
    public den: Denuncia;

    private sub: any;

    constructor(private http: Http, private router: Router, @Inject('BASE_URL') private baseUrl: string, private route: ActivatedRoute) {
        this.sub = this.route.params.subscribe(params => {
            this.id_denuncia = params['id_denuncia'];
        });
        let body = JSON.stringify({ nome: 'Ali' });
        let query = "id_denuncia=" + this.id_denuncia;

        http.post(baseUrl + 'api/Admin/getDenunciabyID?' + query, body).subscribe(result => {
            this.den = result.json() as Denuncia;
            console.log(this.den)
            console.log(this.id_denuncia)
        }, error => console.error(error));
    }

    public BloquearUtilizador(id: number) {
        console.log("id passado: ", id)

        let body = JSON.stringify({ nome: 'Ali' });

        //let query = "nome=" + this.liga + "&" + "desporto=" + this.nome_m;
        let query = "id=" + id;

        this.http.post(this.baseUrl + 'api/Admin/BloquearUtilizador?' + query, body).subscribe(
            data => {
                this.router.navigateByUrl("/admin/denuncias");
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }
    public ResponderDenuncia() {

        let body = JSON.stringify({ nome: 'Ali' });

        //let query = "nome=" + this.liga + "&" + "desporto=" + this.nome_m;
        let query = "respostaAdmin=" + this.resposta + "&id=" + this.id_denuncia;

        this.http.post(this.baseUrl + 'api/Admin/RespostaAdmin?' + query, body).subscribe(
            data => {
                this.router.navigateByUrl("/admin/denuncias");
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }

    public ArquivarDenuncia() {

        let body = JSON.stringify({ nome: 'Ali' });

        //let query = "nome=" + this.liga + "&" + "desporto=" + this.nome_m;
        let query = "id=" + this.id_denuncia;

        this.http.post(this.baseUrl + 'api/Admin/ArquivarDenuncia?' + query, body).subscribe(
            data => {
                this.router.navigate(['/admin/denuncias']);
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }
}

interface Denuncia {
    Id_denuncia: number,
    Id_comentario: number | null,
    Id_aposta: number | null,
    denunciante: string,
    denunciado: string,
    Id_denunciante: number,
    Id_denunciado: number,
    Tipo_Denuncia: number,
    data: Date,
    motivo: string,
    Data_Resposta: Date | null,
    resposta_administrador: string | null,
    estado: boolean,
    conteudoDenunciado: string
}