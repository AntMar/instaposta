﻿import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, Params } from '@angular/router';
import { FormControl } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { OnInit } from '@angular/core';
import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider
} from 'angular5-social-login';
import { User } from '../shared/user.model';
import { RouterLink } from '@angular/router/src/directives/router_link';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css'],
    providers: [UserService]
})
/** register component*/
export class RegisterComponent implements OnInit  {
    public forecasts: Modalidade[];
    ngOnInit(): void {
        throw new Error("Method not implemented.");
    }
    registerForm: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';


    constructor(private fb: FormBuilder, private router: Router, public userService: UserService, private http: Http, @Inject('BASE_URL') baseUrl: string/*, private toastr: ToastrService*/, public socialAuthService: AuthService) {
        http.get(baseUrl + 'api/User/Modalidades').subscribe(result => {
            this.forecasts = result.json() as Modalidade[];
        }, error => console.error(error));
        this.createForm();
    }

    createForm() {
        this.registerForm = this.fb.group({
            email: ['', Validators.email],
            password: ['', Validators.required],
            confirmar_password: ['', Validators.required],
            data_nascimento: [, Validators.required],
            username: ['', Validators.required],
            nome: ['', Validators.required],
            genero: ['', Validators.required],
            modalidade_favorita: ['', Validators.required]
        });
    }

    tryRegister(value: any) {
        if (value.password == value.confirmar_password) {
            if (this.registerForm.valid) {
                this.userService.registerUser(value)
                    .subscribe((data: any) => {
                        switch (data._body) {
                            case "sucesso": {
                                this.errorMessage = "";
                                this.successMessage = "Utilizador registado com sucesso!!!";
                                this.router.navigate(['/login']);
                                break;
                            }
                            case "erro": {
                                this.errorMessage = "Credenciais já existentes!!!";
                                this.successMessage = "";
                                break;
                            }
                            case "idade": {
                                this.errorMessage = "Para se registar necessita de ser maior de idade!!!";
                                this.successMessage = "";
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                    });
            }
            else {
                if (value.genero == "" || value.modalidade_favorita == "" || value.nome == "" || value.username == "" || value.data_nascimento == null || value.password == "" || value.email == "") {
                    this.errorMessage = "Campos de preenchimento obrigatório!!!";
                    this.successMessage = ""
                }
                else {
                      this.errorMessage = "Insira um email válido!!!";
                      this.successMessage = "";
                    
                }

            }
        }
        else {
            this.errorMessage = "As passwords não coincidem!!!";
            this.successMessage = "";
        }

    }
    tryRegisterSocial(socialPlatform: string) {
        let socialPlatformProvider;
        if (socialPlatform == "facebook") {
            socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        } else if (socialPlatform == "google") {
            socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }
        if (socialPlatformProvider != null)
            this.socialAuthService.signIn(socialPlatformProvider).then(
                (userData) => {
                    console.log(socialPlatform + " sign in data : ", userData); 

                    this.userService.SocialRegister(userData.email, userData.image, userData.name, userData.provider, userData.id)
                        .subscribe((data: any) => {
                            if (data._body == "sucesso") {
                                this.errorMessage = "";
                                this.successMessage = "Utilizador registado com sucesso!!!";
                                this.router.navigate(['/login']);
                                }
                            else {
                                this.successMessage = "";
                                this.errorMessage = "Credenciais já existentes!!!";
                            }
                        });
                }
            );

    }
}
interface Modalidade {
    Nome: string;
    IdM: number;
    Foto: string;
}