﻿export class User {
    Id: string;
    UserName: string;
    Password: string;
    Password1: string;
    Password2: string;
    Email: string;
    datanascimento: Date;
    fotoperfil: string;
    nome: string;
    genero: string;
    modalidadefavorita: number;
    provider: string;
    IdFacebook: string;
    IdGoogle: string;
    stars: string;
}

export class Bet {
    Id: number;
    tipoAposa: number;
    modalidade: number;
    liga: number;
    dataInicio: Date;
    dataFim: Date;
    odd: number;
    risco: number;
    descricao: string;
    foto: string;
}

export class Comment {
    idComentario: number;
    idAposta: number;
    comentario: string;
}

export class Denuncia {
    idAposta: number;
    idComentario: number;
    idDenunciante: number;
    idDenunciado: number;
    tipoDenuncia: number;
    motivo: string;
}

export class Avaliacao {
    idAposta: number;
    idUser: number;
    estadoLike: number;
}
