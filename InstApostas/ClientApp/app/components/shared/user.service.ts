﻿import { Injectable, Inject } from '@angular/core';

import { Response, Http } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { User, Comment, Denuncia, Avaliacao } from './user.model';
import { Bet } from './user.model';
import { validateConfig } from '@angular/router/src/config';
import { useAnimation } from '@angular/animations';

@Injectable()
export class UserService {
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) { }

    registerUser(value: any) {
        const body = new User();

        body.UserName = value.username;
        body.Password = value.password;
        body.Email = value.email;
        body.datanascimento=value.data_nascimento,
        body.nome = value.nome;
        body.genero = value.genero;
        body.modalidadefavorita = value.modalidade_favorita

        return this.http.post(this.baseUrl + '/api/User/Register', body);
    }

    LoginUser(value: any) {
        const body = new User();

        body.Password = value.password;
        body.Email = value.email;


        return this.http.post(this.baseUrl + '/api/User/Login', body);
    }
    ResetPassword(value: any) {
        const body = new User();
        body.Email = value.email;
        return this.http.post(this.baseUrl + '/api/User/Resetpass', body);
    }
    SocialLogin(email: string, image: string, name: string) {

        const body = new User();
        body.Email = email;
        body.nome = name;
        body.fotoperfil = image;
        return this.http.post(this.baseUrl + '/api/User/SocialLogin', body);
    }
    SocialRegister(email: string, image: string, name: string, provider: string, id: string) {

        const body = new User();
        body.Email = email;
        body.nome = name;
        body.fotoperfil = image;
        body.provider = provider;
        if (provider == "facebook") {
            body.IdFacebook = id;
            body.IdGoogle = "";
        }
        else {
            body.IdGoogle = id;
            body.IdFacebook = "";
        }
        return this.http.post(this.baseUrl + '/api/User/SocialRegister', body);
    }

    ConfirmarEmail(id: any) {
        const body = new User();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/User/ConfirmarEmail', body);
    }

    //

    UserPerfil(id: number) {
        const body = new User();

        body.Id = id.toString();
        return this.http.post(this.baseUrl + '/api/Perfil/Util', body);     
    }

    UserSeguidores(id: number) {
        const body = new User();

        body.Id = id.toString();
        return this.http.post(this.baseUrl + '/api/Perfil/Seguidores', body);
    }

    UserAseguir(id: number) {
        const body = new User();

        body.Id = id.toString();
        return this.http.post(this.baseUrl + '/api/Perfil/Aseguir', body);
    }

    UserApostasA(id: number) {
        const body = new User();

        body.Id = id.toString();
        return this.http.post(this.baseUrl + '/api/Perfil/Apostasa', body);
    }

    UserApostasH(id: number) {
        const body = new User();

        body.Id = id.toString();
        return this.http.post(this.baseUrl + '/api/Perfil/Apostash', body);
    }

    SeguirUtil(idSeguido:number) {
        const body = new User();
        body.Id = idSeguido.toString();
                
        return this.http.post(this.baseUrl + '/api/Perfil/SeguirUtil', body);
    }

    Versesegue(idSeguido: number) {
        const body = new User();
        body.Id = idSeguido.toString();

        return this.http.post(this.baseUrl + '/api/Perfil/Versesegue', body);
    }

    VisualizarAposta(id: number) {
        const body = new Bet();

        body.Id = id;
        return this.http.post(this.baseUrl + '/api/Aposta/VisualizarAposta', body);
    }

    CarregarComentarios(id: number) {
        const body = new Bet();

        body.Id = id;
        return this.http.post(this.baseUrl + '/api/Aposta/CarregarComentarios', body);
    }

    BuscarTipoAposta(id: number) {
        const body = new Bet();

        body.Id = id;
        
        return this.http.post(this.baseUrl + '/api/Aposta/CarregarTipoAposta', body);
    }

    buscarModalidade(id: number) {
        const body = new Bet();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Aposta/CarregarModalidade', body);
    }

    buscarLiga(id: number) {
        const body = new Bet();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Aposta/CarregarLiga', body);
    }

    buscarDescricao(id: number) {
        const body = new Bet();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Aposta/CarregarDescricao', body);
    }

    buscarOdd(id: number) {
        const body = new Bet();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Aposta/CarregarOdd', body);
    }

    buscarRisco(id: number) {
        const body = new Bet();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Aposta/CarregarRisco', body);
    }

    buscarDonoApostaId(id: number) {
        const body = new Bet();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Aposta/DonoApostaId', body);
    }

    buscarDonoApostaUsername(id: number) {
        const body = new Bet();

        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Aposta/DonoApostaUsername', body);
    }

    inserirComentario(value: any, id: number) {
        const body = new Comment();

        body.idAposta = id;
        body.comentario = value.comentario;

        return this.http.post(this.baseUrl + '/api/Aposta/InserirComentario', body);
    }

    inserirDenunciaAposta(value: any, id: number, donoApostaId: number) {
        const body = new Denuncia();

        body.idAposta = id;
        body.motivo = value.denuncia;
        body.tipoDenuncia = 1;
        body.idDenunciado = donoApostaId;

        return this.http.post(this.baseUrl + '/api/Aposta/inserirDenunciaAposta', body);
    }

    inserirDenunciaComentario(value: any, id: number, utilizadorComentarioId: number) {
        const body = new Denuncia();

        body.idDenunciado = utilizadorComentarioId;
        body.idComentario = id;
        body.motivo = value.motivo;
        body.tipoDenuncia = 0;

        return this.http.post(this.baseUrl + '/api/Aposta/inserirDenunciaComentario', body);
    }

    verificarLike(idAposta: number) {
        const body = new Avaliacao();

        body.idAposta = idAposta;

        return this.http.post(this.baseUrl + '/api/Aposta/verificarLike', body);
    }

    carregarLike(idAposta: number) {
        const body = new Avaliacao();

        body.idAposta = idAposta;
        return this.http.post(this.baseUrl + '/api/Aposta/carregarLike', body);
    }

    totalLikes(id: number) {
        const body = new Bet();

        body.Id = id;
        return this.http.post(this.baseUrl + '/api/Apostas/TotalLikes', body);
    }

    upload(fileToUpload: any) {
        let input = new FormData();
        input.append("file", fileToUpload);
        return this.http.post(this.baseUrl + '/api/Apostas/Foto', input);
    }

    uploadFotoUser(fileToUpload: any) {
        let input = new FormData();
        input.append("file", fileToUpload);
        return this.http.post(this.baseUrl + '/api/Perfil/Foto', input);
    }


    EliminarAposta(id: number) {
        const body = new Bet();
        body.Id = id;

        return this.http.post(this.baseUrl + '/api/Perfil/ElimAposta', body);
    }

    EditarUtil(value: any,id:number) {
        const body = new User();

        body.Id = id.toString();
        body.nome = value.nome;
        body.UserName = value.username;
        body.Email=value.mail
        body.Password = value.pass;
        body.Password1 = value.pass1;
        body.Password2 = value.pass2;
        body.modalidadefavorita = value.modalidade;

        return this.http.post(this.baseUrl + '/api/Perfil/Editar', body);
    }

    MeteStars(stars: number, id: number) {
        const body = new User();
        body.Id = id.toString();
        body.stars = stars.toString();

        return this.http.post(this.baseUrl + '/api/Perfil/Stars', body);
    }

    VerEstrelas(id: number) {
        const body = new User();
        body.Id = id.toString();       
        return this.http.post(this.baseUrl + '/api/Perfil/VerStars', body);
    }

    GetOddMedia(id: number) {
        const body = new User();
        body.Id = id.toString();

        return this.http.post(this.baseUrl + '/api/Perfil/Odd', body);
    }

    GetAvaliacaoMedia(id: number) {
        const body = new User();
        body.Id = id.toString();

        return this.http.post(this.baseUrl + '/api/Perfil/Avaliacao', body);
    }

    denunciarUtilizador(value: any, id: number) {
        const body = new Denuncia();
        body.idDenunciado = id;
        body.motivo = value.denuncia;
        body.tipoDenuncia = 2;

        return this.http.post(this.baseUrl + '/api/Perfil/DenunciarUser', body);
    }
}