﻿import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { HttpModule } from '@angular/http';

import { User } from '../shared/user.model';
import { RenderToStringResult } from 'aspnet-prerendering';
import { using } from 'rxjs/observable/using';
import { DatePipe } from '@angular/common';
import { ElementData } from '@angular/core/src/view';
import { query } from '@angular/core/src/animation/dsl';

@Component({
    selector: 'perfil',
    templateUrl: './perfil.component.html',
    providers: [UserService]
})



export class Perfil {

    id: number;
    private sub: any;
    public userLogin: Utilizador;
    public empList: Utilizador;
    public ListApostasA: Aposta[];
    public ListApostasH: Aposta[];
    public seguidores: number;
    public aseguir: number;
    public Segue: string;
    public simbolo: string;
    public Stars: string="";
    public star1: string;
    public star2: string;
    public star3: string;
    public star4: string;
    public star5: string;
    public Nestrelas: string;
    public Odd: number;
    public Avaliacao: number;

    constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, public userService: UserService, private http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/User/CurrentUser').subscribe(result => {
           this.userLogin = result.json() as Utilizador ;
        }, error => console.error(error));


        
    }

    ngOnInit() {
        this.sub = this.route
            .queryParams
            .subscribe(params => {
                this.id = +params['user'];
            });
        this.carregarPerfil(this.id);      
        this.carregarSeguidores(this.id);
        this.carregarAseguir(this.id);
        this.versesegue(this.id);
        this.carregarApostasA(this.id);
        this.carregarApostasH(this.id);
        this.verStars(this.id);
        this.OddMedia(this.id);
        this.AvaliacaoMedia(this.id);
    }

    carregarPerfil(id: number) {
        this.userService.UserPerfil(id)
            .subscribe(data => this.empList = data.json() as Utilizador)
    }

    carregarSeguidores(id: number) {
        this.userService.UserSeguidores(id)
            .subscribe(data => this.seguidores = data.json() as number)
    }

    carregarAseguir(id: number) {
        this.userService.UserAseguir(id)
            .subscribe(data => this.aseguir = data.json() as number)
    }

    carregarApostasA(id: number) {
        this.userService.UserApostasA(id)
            .subscribe(data => this.ListApostasA = data.json() as Aposta[])
    }

    carregarApostasH(id: number) {
        this.userService.UserApostasH(id)
            .subscribe(data => this.ListApostasH = data.json() as Aposta[])
    }

    seguirUtil(id: number) {
        this.userService.SeguirUtil(id)
        .subscribe(result => {
            this.Segue = result.json() as string;
            switch (this.Segue) {
                case "segue": {
                    this.simbolo= "1";
                    this.carregarSeguidores(this.id);
                    this.carregarAseguir(this.id);
                    console.log(this.star3);
                    break;
                }              
                case "naosegue": {                    
                    this.simbolo = "0";
                    this.carregarSeguidores(this.id);
                    this.carregarAseguir(this.id);
                    break;
                }
                default: {
                    break;
                }
            }
        })          
    }

    versesegue(id: number) {
        this.userService.Versesegue(id)
            .subscribe(result => {
                this.Segue = result.json() as string;
                switch (this.Segue) {
                    case "segue": {
                        this.simbolo = "1";                        
                        break;
                    }
                    case "naosegue": {
                        this.simbolo = "0";                        
                        break;
                    }
                    default: {
                        break;
                    }
                }
            })
    }

    tryStars(stars: number) {
        this.userService.MeteStars(stars,this.id)
            .subscribe(result => {
                this.Stars = result.json() as string;
                switch (this.Stars) {
                    case "ok": {
                        this.verStars(this.id);
                        this.AvaliacaoMedia(this.id);
                        break;
                    }                    
                    default: {
                        break;
                    }
                }
            })
    }
    

    verStars(id: number) {
        this.userService.VerEstrelas(id)
            .subscribe(result => {
                this.Nestrelas = result.json() as string;
                switch (this.Nestrelas) {
                    case "0": {                        
                        break;
                    }
                    case "1": {
                        this.star1 = "true";
                        this.star2 = "";
                        this.star3 = "";
                        this.star4 = "";
                        this.star5 = "";
                        break;
                    }
                    case "2": {
                        this.star2 = "true";
                        this.star1 = "";
                        this.star3 = "";
                        this.star4 = "";
                        this.star5 = "";
                        break;
                    }
                    case "3": {
                        this.star3 = "true";
                        this.star1 = "";
                        this.star2 = "";
                        this.star4 = "";
                        this.star5 = "";
                        break;
                    }
                    case "4": {
                        this.star4 = "true";
                        this.star1 = "";
                        this.star2 = "";
                        this.star3 = "";
                        this.star5 = "";
                        break;
                    }
                    case "5": {
                        this.star5 = "true";
                        this.star1 = "";
                        this.star2 = "";
                        this.star3 = "";
                        this.star4 = "";
                        break;
                    }
                    default: {
                        break;
                    }
                }
            })
    }

    OddMedia(id: number) {
        this.userService.GetOddMedia(id)
            .subscribe(result => {
                this.Odd = result.json() as number;              
            })
    }

    AvaliacaoMedia(id: number) {
        this.userService.GetAvaliacaoMedia(id)
            .subscribe(result => {
                this.Avaliacao = result.json() as number;
            })
    }

    public mostrarDenunciar = false;

    public denunciarUser() {
        this.mostrarDenunciar = true;
        this.createForm();
    }

    denunciaForm: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';

    createForm() {
        this.denunciaForm = this.fb.group({
            denuncia: ['', Validators.required],
        });
    }

    tryDenuncia(value: any) {
        if (this.denunciaForm.valid) {
            console.log("ola");
            this.userService.denunciarUtilizador(value, this.id)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.successMessage = "Denuncia submetida com sucesso!";
                        }
                        default: {
                            break;
                        }
                    }
                });
        }
        else {
            if (value.denuncia == "") {
                this.errorMessage = "Campos de preenchimento obrigatório!!!";
                this.successMessage = ""
            }

        }
    }

    cancelarDenuncia() {
        this.mostrarDenunciar = false;
    }
}



interface Utilizador {
    id: number;
    username: string;
    nome: string;
    fotoPerfil: string;
}

interface Aposta {
    id: number;
    dataFim:Date;
    foto: string;   
}
