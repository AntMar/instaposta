﻿import { Component, Inject, ViewChild } from '@angular/core';
import { NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { Form } from '@angular/forms/src/directives/form_interface';
import { Http } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import { Bet } from '../shared/user.model';
import { UserService } from '../shared/user.service';

@Component({
    selector: 'inserirAposta',
    templateUrl: './inserirAposta.component.html',
    providers: [UserService]
})

export class InserirAposta {

   
    public modalidade: Modalidade[];

    public liga: Liga[];

    public userLogado: Utilizador;

    apostaForm: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';

    constructor(private fb: FormBuilder, private router: Router, public userService: UserService, private http: Http, @Inject('BASE_URL') public baseUrl: string) {
        http.get(baseUrl + 'api/Aposta/Modalidades').subscribe(result => {
            this.modalidade = result.json() as Modalidade[];
        }, error => console.error(error));

        http.get(baseUrl + 'api/Aposta/Liga').subscribe(result => {
            this.liga = result.json() as Liga[];
        }, error => console.error(error));

        http.get(baseUrl + 'api/User/CurrentUser').subscribe(result => {
            this.userLogado = result.json() as Utilizador;
        }, error => console.error(error));

        this.createForm();
        this.visibilidade = "hidden"

    }

    createForm() {
        this.apostaForm = this.fb.group({
            tipoAposta: ['', Validators.required],
            modalidade: ['', Validators.required],
            liga: ['', Validators.required],
            dataInicio: [, Validators.required],
            odd: ['', Validators.required],
            risco: ['', Validators.required],
            descricao: ['', Validators.required],
            foto: ['', Validators.required],
            dataFim: ['', Validators.required]
        });
    }

    @ViewChild("fileInput") fileInput: any;

    tryInserirAposta(value: any) {
        if (this.apostaForm.valid) {
            this.registarAposta(value)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.errorMessage = "";
                            this.successMessage = "Aposta inserida com sucesso!";
                            let fi = this.fileInput.nativeElement;
                            if (fi.files && fi.files[0]) {
                                let fileToUpload = fi.files[0];
                                this.userService.upload(fileToUpload)
                                    .subscribe(res => {
                                        console.log(res);
                                    });
                            }
                            //this.router.navigate(['/perfil']);
                            break;
                        }
                        case "data": {
                            this.errorMessage = "Confirme as datas das apostas!";
                            this.successMessage = "";
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                });
           
        }
        else {
            if (value.tipoAposta == "" || value.dataInicio == "" || value.dataFim == "" || value.descricao == "" || value.foto == "" || value.liga == null || value.modalidade == "" || value.odd == "" || value.risco == "") {
                this.errorMessage = "Campos de preenchimento obrigatório!!!";
                this.successMessage = ""
            }

        }
    }

   

    registarAposta(value: any) {
        const body = new Bet();

        body.tipoAposa = value.tipoAposta;
        body.dataInicio = value.dataInicio;
        body.dataFim = value.dataFim;
        body.descricao = value.descricao;
        body.foto = value.foto;
        body.liga = value.liga;
        body.modalidade = value.modalidade;
        body.odd = value.odd;
        body.risco = value.risco;
        return this.http.post(this.baseUrl + '/api/Aposta/InserirAposta', body);
    }
    
    public visibilidade: string;
    public ligaespecial: Liga[];

    public ativar(id: any) {
        
    }




    public percentagem = 0;
    public cor = "white";

    public alterarRisco(val : any) {
        this.percentagem = val;
        console.log(val);

        if (val == "")
            this.percentagem = 0;

        if (val > 70)
            this.cor = "green";
        else
            if (val > 50 && val < 70)
                this.cor = "#FFD300";
            else {
                this.cor = "red";    
            }
                
    }

    private mostrar = true;

    public onSubmit(f: NgForm) {
        //this.mostrar = !this.mostrar;
        f.value.odd
    }

    public submeterOutra() {
        this.mostrar = !this.mostrar;
    }

    public visualizarAposta() {

    }

    public irParaVisualizarAposta() {
        this.router.navigateByUrl('/visualizarAposta');
    };

    //getButtonText(): string {
    //    return `Switch ${this.mostrar ? 'Off' : 'On'}`;
    //}



}

interface Modalidade {
    Nome: string;
    IdM: number;
    Foto: string;
}

interface Liga {
    Nome: string;
    IdL: number;
    IdM: number;
}

interface Utilizador {
    id: number;
    username: string;
    nome: string;
    fotoPerfil: string;
}