﻿import { Component, Inject } from '@angular/core';
import { NgForm, Validators, FormGroup, FormBuilder } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { Form } from '@angular/forms/src/directives/form_interface';
import { Http } from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import { Bet } from '../shared/user.model';
import { UserService } from '../shared/user.service';
import { User } from '../shared/user.model';

@Component({
    selector: 'visualizarAposta',
    templateUrl: './visualizarAposta.component.html',
    providers: [UserService]
})
export class VisualizarAposta {

    private sub: any;
    public id: number;
    //public aposta: Aposta;
    public tipoAposta: string;
    public modalidade: string;
    public liga: string;
    public odd: number;
    public risco: number;
    public descricao: string;
    public userLogado: Utilizador;
    public donoApostaId: number;
    public donoApostaUsername: string;
    public comentarios: Comentarios[];
    public aposta: Aposta;
    public utilizadorComentarioId: number;
    public likes: number;

    constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, public userService: UserService, private http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/User/CurrentUser').subscribe(result => {
            this.userLogado = result.json() as Utilizador;
        }, error => console.error(error));
    }

    ngOnInit() {
        this.sub = this.route
            .queryParams
            .subscribe(params => {
                this.id = +params['aposta'];
            });

        this.totalLikes(this.id);
        this.verificarLike(this.id);
        this.verificarRisco();
        this.carregarAposta(this.id);
        this.carregarComentarios(this.id);
        this.buscarTipoAposta(this.id);
        this.buscarModalidade(this.id);
        this.buscarLiga(this.id);
        this.buscarDescricao(this.id);
        this.buscarOdd(this.id);
        this.buscarRisco(this.id);
        this.buscarDonoApostaId(this.id);
        this.buscarDonoApostaUsername(this.id);

    }

    carregarAposta(id: number) {
        this.userService.VisualizarAposta(id)
            .subscribe(data => this.aposta = data.json() as Aposta)
        
    }

    totalLikes(id: number) {
        this.userService.totalLikes(id)
            .subscribe(data => this.likes = data.json() as number)
    }

    //buscarDataFim(id: number) {
    //    this.userService.buscarDonoApostaId(id)
    //        .subscribe(data => this.donoApostaId = data.json() as number)
    //}

    //buscarDataInicio(id: number) {
    //    this.userService.buscarDonoApostaId(id)
    //        .subscribe(data => this.donoApostaId = data.json() as number)
    //}

    buscarDonoApostaId(id: number) {
        this.userService.buscarDonoApostaId(id)
            .subscribe(data => this.donoApostaId = data.json() as number)
    }

    buscarDonoApostaUsername(id: number) {
        this.userService.buscarDonoApostaUsername(id)
            .subscribe(data => this.donoApostaUsername = data.text())
    }

    buscarTipoAposta(id: number) {
         this.userService.BuscarTipoAposta(id)
             .subscribe(data => this.tipoAposta = data.text())
    }

    buscarModalidade(id: number) {
        this.userService.buscarModalidade(id)
            .subscribe(data => this.modalidade = data.text())
    }

    buscarLiga(id: number) {
        this.userService.buscarLiga(id)
            .subscribe(data => this.liga = data.text())
    }

    buscarDescricao(id: number) {
        this.userService.buscarDescricao(id)
            .subscribe(data => this.descricao = data.text())
    }

    buscarOdd(id: number) {
        this.userService.buscarOdd(id)
            .subscribe(data => this.risco = data.json() as number)
    }

    buscarRisco(id: number) {
        this.userService.buscarRisco(id)
            .subscribe(data => this.odd = data.json() as number)
    }

    carregarComentarios(id: number) {
        this.userService.CarregarComentarios(id)
            .subscribe(data => this.comentarios = data.json() as Comentarios[])
    }

    /* MERDAS */
    public largura = 300;
    public altura = 300;

    public zoom() {
        this.largura = 600;
        this.altura = 600;
    }

    public zoomOver() {
        this.largura = 300;
        this.altura = 300;
    }
    /* MERDAS */

    public mudarCor = "black";
    public meteulike: number;

    public Result1: string;

    verificarLike(idAposta: number) {
        this.userService.verificarLike(idAposta)
            .subscribe((result => {
                this.Result1 = result.text();
                switch (this.Result1) {
                    case "sucesso": {
                        this.mudarCor = "red";
                        this.meteulike = 1;
                        break;
                    }
                    case "erro": {
                        this.mudarCor = "black";
                        this.meteulike = 0;
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }));
    }

    public Result2: string;

    carregarLike(idAposta: number) {
        this.userService.carregarLike(idAposta)
            .subscribe((result => {
                this.Result2 = result.text();
                switch (this.Result2) {
                    case "sucesso": {
                        this.verificarLike(this.id);
                        this.totalLikes(this.id);
                        break;
                    }
                    case "erro": {
                        this.errorMessage = "Ocorreu erro -333";
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }));
    }

    public like() {
        this.carregarLike(this.id);
    }

    public cor = "red";

    public verificarRisco() {
        if (this.risco > 70)
            this.cor = "green";
        else
            if (this.risco > 50 && this.risco < 70)
                this.cor = "#FFD300";
            else {
                this.cor = "red";
            }
    }



    public mostrarComentar = false;

    public comentar() {
        console.log(this.likes);
        this.mostrarComentar = true;
        this.createFormComentario();
    }

    public cancelarComentario() {
        this.mostrarComentar = false;
    }

    comentarioForm: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';

    createFormComentario() {
        this.comentarioForm = this.fb.group({
            comentario: ['', Validators.required],
        });
    }

    tryInserirComentario(value: any) {
        if (this.comentarioForm.valid) {
            this.userService.inserirComentario(value, this.id)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.carregarComentarios(this.id);
                        }
                        default: {
                            break;
                        }
                    }
                });
        }
        else {
            if (value.comentario == "") {
                this.errorMessage = "Campos de preenchimento obrigatório!!!";
                this.successMessage = ""
            }

        }
    }

    denunciaApostaForm: FormGroup;
    errorMessageDenunciaAposta: string = '';
    successMessageDenunciaAposta: string = '';

    createFormDenunciaAposta() {
        this.denunciaApostaForm = this.fb.group({
            denuncia: ['', Validators.required],
        });
    }

    tryInserirDenunciaAposta(value: any) {
        if (this.denunciaApostaForm.valid) {
            this.userService.inserirDenunciaAposta(value, this.id, this.donoApostaId)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.successMessageDenunciaAposta = "Aposta foi denunciada com sucesso!";
                        }
                        default: {
                            break;
                        }
                    }
                });
        }
        else {
            if (value.denuncia == "") {
                this.errorMessageDenunciaAposta = "Campos de preenchimento obrigatório!!!";
                this.successMessageDenunciaAposta = ""
            }

        }
    }

    public mostrarDenunciarAposta = false;

    public denunciarAposta() {
        this.mostrarDenunciarAposta = true;
        this.createFormDenunciaAposta();
    }

    public cancelarDenunciaAposta() {
        this.mostrarDenunciarAposta = false;
    }

    denunciaComentarioForm: FormGroup;
    errorMessageDenunciaComentario: string = '';
    successMessageDenunciaComentario: string = '';

    createFormDenunciaComentario() {
        this.denunciaComentarioForm = this.fb.group({
            motivo: ['', Validators.required],
        });
    }
    public idCom: number;
    tryInserirDenunciaComentario(value: any) {
        if (this.denunciaComentarioForm.valid) {
            this.userService.inserirDenunciaComentario(value, this.idCom, this.utilizadorComentarioId)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.successMessageDenunciaComentario = "Comentario foi denunciado com sucesso!";
                        }
                        default: {
                            break;
                        }
                    }
                });
        }
        else {
            if (value.motivo == "") {
                this.errorMessageDenunciaComentario = "Campos de preenchimento obrigatório!!!";
                this.successMessageDenunciaComentario = ""
            }

        }
    }

    public mostrarDenunciarComentario = false;


    public denunciarComentario(id: number, idUtilizador: number) {
        this.idCom = id;
        this.utilizadorComentarioId = idUtilizador;
        this.mostrarDenunciarComentario = true;
        this.createFormDenunciaComentario();
    }

    public cancelarDenunciaComentario() {
        this.mostrarDenunciarComentario = false;
    }

}

interface Utilizador {
    id: number;
    username: string;
    nome: string;
    fotoPerfil: string;
}

//interface Aposta {
//    tipoAposta: number;
//    modalidade: number;
//    liga: number;
//    odd: number;
//    risco: number;
//    descricao: string;
//}

interface Comentarios {
    idComentario: number;
    data: Date;
    utilizador: string;
    descricao: string;
}

interface Aposta {
    iD: number;
    data_Inicio: Date;
    data_Fim: Date;
    foto: string;
    c2: string;
    c3: string;
    c4: string;
    c5: string;
    c6: string;
    c7: string;
}
