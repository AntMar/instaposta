﻿import { Component, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { NgForm } from '@angular/forms';
import { Http, RequestOptions } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { HttpModule } from '@angular/http';

import { User } from '../shared/user.model';
import { RenderToStringResult } from 'aspnet-prerendering';
import { using } from 'rxjs/observable/using';
import { DatePipe } from '@angular/common';
import { ElementData } from '@angular/core/src/view';
import { query } from '@angular/core/src/animation/dsl';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'editarperfil',
    templateUrl: './editarperfil.component.html',
    providers: [UserService]
})
export class EditarPerfil {

    id: number;
    private sub: any;
    public userLogin: Utilizador;
    public ListApostasA: Aposta[];
    public Elim: string;
    public Result: string;
    public modalidade: Modalidade[];

    editarForm: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';


    constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, public userService: UserService, private http: Http, @Inject('BASE_URL') public baseUrl: string) {

        http.get(baseUrl + 'api/User/CurrentUser').subscribe(result => {
            this.userLogin = result.json() as Utilizador;
        }, error => console.error(error));

        http.get(baseUrl + 'api/User/Modalidades').subscribe(result => {
            this.modalidade = result.json() as Modalidade[];
        }, error => console.error(error));
        this.createForm();
    }

    ngOnInit() {
        
        this.sub = this.route
            .queryParams
            .subscribe(params => {
                this.id = +params['user'];
            });
        this.carregarApostasA(this.id);
        
    }

    createForm() {
        this.editarForm = this.fb.group({
            nome: [''],
            username: [''],
            mail: [''],
            pass: [''],
            pass1: [''],
            pass2: [''],
            modalidade: [''],            
        });

    }

    tryEditar(value: any) {
        if (this.editarForm.valid) {
            this.userService.EditarUtil(value,this.id)
                .subscribe(result => {
                    this.Result = result.json() as string;
                    switch (this.Result) {
                        case "sucesso": {
                            this.errorMessage = "";
                            this.successMessage = "Dados editados com sucesso!";
                            this.carregarPerfil(this.id);
                            break;
                        }
                        case "password": {
                            this.errorMessage = "Password errada!";
                            this.successMessage = "";
                            this.carregarPerfil(this.id);
                            break;
                        }
                        case "password2": {
                            this.errorMessage = "As passwords não coincidem!";
                            this.successMessage = "";
                            this.carregarPerfil(this.id);
                            break;
                        }
                        case "mail": {
                            this.errorMessage = "Email já existente!";
                            this.successMessage = "";
                            this.carregarPerfil(this.id);
                            break;
                        }
                        case "erro": {
                            this.errorMessage = "Ocorreu erro, tente novamente";
                            this.successMessage = "";
                            this.carregarPerfil(this.id);
                            break;

                        }
                        default: {
                            break;
                        }
                    }
                });
        }
        else {
            if (value.tipoAposta == "" || value.dataInicio == "" || value.descricao == "" || value.foto == "" || value.liga == null || value.modalidade == "" || value.odd == "" || value.risco == "") {
                this.errorMessage = "Campos de preenchimento obrigatório!!!";
                this.successMessage = ""
            }

        }
    }

    carregarPerfil(id: number) {
        this.userService.UserPerfil(id)
            .subscribe(data => this.userLogin = data.json() as Utilizador)
    }

    carregarApostasA(id: number) {
        this.userService.UserApostasA(id)
            .subscribe(data => this.ListApostasA = data.json() as Aposta[])
    }


    ElimAposta(id: number) {
        this.userService.EliminarAposta(id).subscribe(result => {
            this.Elim = result.json() as string;
            switch (this.Elim) {
                case "true": {
                    
                    this.carregarApostasA(this.id);
                    break;
                }
                default: {
                    break;
                }
            }
        }) 
    }

    @ViewChild("fileInput") fileInput: any;

    uploadFoto() {
        console.log("entra");
        let fi = this.fileInput.nativeElement;
        if (fi.files && fi.files[0]) {
            let fileToUpload = fi.files[0];
            this.userService.uploadFotoUser(fileToUpload)
                .subscribe(res => {
                    console.log(res);
                });
        }
        location.reload();
    }

}

interface Utilizador {
    id: number;
    username: string;
    nome: string;
    fotoPerfil: string;
    email: string;
}

interface Aposta {
    id: number;
    dataFim: Date;
    foto: string;
}

interface Modalidade {
    Nome: string;
    IdM: number;
    Foto: string;
}