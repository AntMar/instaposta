﻿import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, Params } from '@angular/router';
import { UserService } from '../shared/user.service'
import { Response } from '@angular/http/src/static_response';
import { Request } from '@angular/http/src/static_request';
@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.css'],
    providers: [UserService]
})
/** reset-password component*/
export class ResetPasswordComponent {

    resetForm: FormGroup;
    errorMessage: string = '';
    successMessage: string = '';
    constructor(private fb: FormBuilder, private router: Router, public userService: UserService) {
        this.createForm();
    }

    createForm() {
        this.resetForm = this.fb.group({
            email: ['', Validators.email]
        });
    }
    tryResetPassword(value: any) {

        if (this.resetForm.valid) {
            this.userService.ResetPassword(value)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.successMessage = "Sucesso a enviar o email, por favor verifique a sua caixa de correio!";
                            this.errorMessage = "";
                            break;
                        }
                        case "erro": {
                            this.errorMessage = "Email inexistente!";
                            this.successMessage = "";
                            break;
                        }
                        case "facebook": {
                            this.errorMessage = "Como efetuou registo com o facebook não é possível recuperar a password!!!";
                            this.successMessage = "";
                            break;
                        }
                        case "google": {
                            this.errorMessage = "Como efetuou registo com o google não é possível recuperar a password!!!";
                            this.successMessage = "";
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    
                });
        }
        else {
            if (value.email == "") {
                this.errorMessage = "Campo de preenchimento obrigatório!"
            }
            else {
                this.errorMessage = "Introduza um email válido!";
            }
        }


    }
}