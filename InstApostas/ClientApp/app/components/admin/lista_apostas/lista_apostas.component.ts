﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Text } from '@angular/compiler';


@Component({
    selector: 'app-lista_apostas',
    templateUrl: './lista_apostas.component.html',
    styleUrls: ['./lista_apostas.component.css']
})


export class Lista_apostasComponent {

    public l_ap: Aposta[];

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string) {
        http.get(baseUrl + 'api/Admin/getApostas').subscribe(result => {
            this.l_ap = result.json() as Aposta[];
        }, error => console.error(error));
    }
}

interface Aposta {
    descricao: string,
    data_inicio: string,
    data_fim: string,
    grau_conf: number,
    id_ut: string,
    odd_total: number,
    ganha: boolean
}
