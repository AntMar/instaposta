﻿import { Component, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { Router } from "@angular/router";

@Component({
    selector: 'app-inserir_desporto',
    templateUrl: './inserir_desporto.component.html',
    styleUrls: ['./inserir_desporto.component.css']
})

export class Inserir_desportoComponent {

    public mod: Modalidades[];

    modalid = "";
    liga = "";

    constructor(private http: Http, private router: Router, @Inject('BASE_URL') private baseUrl: string) {

        http.get(baseUrl + 'api/Admin/getModalidades').subscribe(result => {
            this.mod = result.json();
        }, error => console.error(error));

    }

    public AdicionarLiga() {

        let body = JSON.stringify({ nome: 'Ali' });

        let query = "desporto=" + this.modalid + "&" + "nome=" + this.liga;
        //let query = "nome=" + this.liga;

        this.http.post(this.baseUrl + 'api/Admin/InserirLiga?' + query, body).subscribe(
            data => {
                this.router.navigate(['/admin/ligas']);
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }
   
}

interface Modalidades {
    nome: string
}
