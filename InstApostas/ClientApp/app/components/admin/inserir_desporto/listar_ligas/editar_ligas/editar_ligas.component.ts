﻿import { Component, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-editar_ligas',
    templateUrl: './editar_ligas.component.html',
    styleUrls: ['./editar_ligas.component.css']
})
/** editar_ligas component*/
export class Editar_ligasComponent {
    
    ligaeditar: String;
    public id_liga: number;
    edit_l: Liga;
    private sub: any;

    constructor(private http: Http, private router: Router, @Inject('BASE_URL') private baseUrl: string, private route: ActivatedRoute) {
        this.sub = this.route.params.subscribe(params => {
            this.id_liga = params['id_liga'];
        });

        let query = "id_liga=" + this.id_liga;
        let url = baseUrl + 'api/Admin/getLigaPorID?' + query
        
        http.get(url).subscribe(result => {
            this.edit_l = result.json() as Liga;
        }, error => console.error(error));

        
    }

    public EditarNomeLiga() {

        let body = JSON.stringify({ nome: 'Ali' });

        //let query = "nome=" + this.liga + "&" + "desporto=" + this.nome_m;
        let query = "id=" + this.id_liga + "&nome_editar=" + this.ligaeditar;
        

        this.http.post(this.baseUrl + 'api/Admin/EditarLiga?' + query, body).subscribe(
            data => {
                this.router.navigate(['/admin/ligas']);
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }
}

interface Liga {
    id_liga: number,
    liga: string,
    modalidade: string
}