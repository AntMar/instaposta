﻿import { Component, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
    selector: 'app-listar_ligas',
    templateUrl: './listar_ligas.component.html',
    styleUrls: ['./listar_ligas.component.css']
})
/** listar_ligas component*/
export class Listar_ligasComponent {
    /** listar_ligas ctor */

    public ligas: Liga[];
    private sub: any;

    constructor(private http: Http, private router: Router, @Inject('BASE_URL') private baseUrl: string, private route: ActivatedRoute) {
        

        http.get(baseUrl + 'api/Admin/getLigas').subscribe(result => {
            this.ligas = result.json() as Liga[];
        }, error => console.error(error));
    }
    public getImagem(modalidade: String) {
        if (modalidade == 'Futebol') {
            return "https://image.flaticon.com/icons/svg/33/33736.svg";

        }
        else if (modalidade == 'Basquetebol') {
            return "https://image.flaticon.com/icons/svg/841/841740.svg";

        }
        else if (modalidade == 'Tenis') {
            return "https://image.flaticon.com/icons/svg/108/108649.svg";

        }
        else {
            return "https://image.flaticon.com/icons/svg/25/25251.svg";

        }

    }

    public RemoverLiga(id: number) {

        let body = JSON.stringify({ nome: 'Ali' });
        
        let query = "id=" + id;

        this.http.post(this.baseUrl + 'api/Admin/RemoverLiga?' + query, body).subscribe(
            data => {
                window.location.reload();
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )
        
    }

}

interface Liga {
    id_liga: number,
    liga: string,
    modalidade: string
}