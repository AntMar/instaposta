﻿//import { Component, ElementRef, ViewChild,Inject } from '@angular/core';
//import { Http } from '@angular/http';
//import { chart } from 'highcharts';
//import * as Highcharts from 'highcharts';

//@Component({
//    selector: 'app-home_admin',
//    templateUrl: './home_admin.component.html',
//    styleUrls: ['./home_admin.component.css']
//})
/** home_admin component*/
export class Home_adminComponent {
    /** home_admin ctor */

    //@ViewChild('chartTarget') chartTarget: ElementRef;
    //chart: Highcharts.ChartObject;

    public totais: Totais;

    //constructor(http: Http, @Inject('BASE_URL') baseUrl: string) {

        //http.get(baseUrl + 'api/Admin/getTotais').subscribe(result => {
        //    this.totais = result.json() as Totais;
        //}, error => console.error(error), () => this.constroigraph());


    //}

    //public constroigraph() {
    //    const options: Highcharts.Options = {
    //        chart: {
    //            type: 'column'
    //        },
    //        title: {
    //            text: 'Estatísticas das Denuncias'
    //        },
    //        colors: ['gray'],
    //        xAxis: {
    //            categories: ['Fechadas', 'Abertas']
    //        },
    //        yAxis: {
    //            title: {
    //                text: 'Número de Denuncias'
    //            }
    //        },
    //        series: [{
    //            name: 'Denuncias',
    //            data: [this.totais.den_fechadas, this.totais.den_abertas]
    //        }]
    //    };
    //    this.chart = chart(this.chartTarget.nativeElement, options);

    //}

}

interface Totais {
    den_abertas: number;
    den_fechadas: number;
    den_total: number;
    num_utilizadores: number;
    num_apostas: number;
}




   
