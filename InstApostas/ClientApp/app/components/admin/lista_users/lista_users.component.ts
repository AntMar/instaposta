﻿import { Text } from '@angular/compiler';
import { Component, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';
import { URLSearchParams } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";


@Component({
    selector: 'app-lista_users',
    templateUrl: './lista_users.component.html',
    styleUrls: ['./lista_users.component.css']
})
/** lista_users component*/
export class Lista_usersComponent {

    public l_u: Utilizador[];
    
    constructor(private http: Http, private router: Router, @Inject('BASE_URL') private baseUrl: string, private route: ActivatedRoute) {

        http.get(baseUrl + 'api/Admin/getUsers').subscribe(result => {
            this.l_u = result.json() as Utilizador[];
        }, error => console.error(error));
    }

    public AlterarEstado(id: string) {
        console.log("user: ", id)

        let body = JSON.stringify({ nome: 'Ali' });

        let query = "username=" + id;

        this.http.post(this.baseUrl + 'api/Admin/AlterarEstadoUtilizador?' + query, body).subscribe(
            data => {
                window.location.reload();
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }
}




interface Utilizador {
    username: string,
    nome: string,
    email: string,
    genero: string, 
    estado: number
}