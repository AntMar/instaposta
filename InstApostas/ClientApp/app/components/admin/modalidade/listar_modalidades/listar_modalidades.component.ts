﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Text } from '@angular/compiler';
import { Router } from "@angular/router";

@Component({
    selector: 'app-listar_modalidades',
    templateUrl: './listar_modalidades.component.html',
    styleUrls: ['./listar_modalidades.component.css']
})
/** listar_modalidades component*/
export class Listar_modalidadesComponent {
    /** listar_modalidades ctor */

    public modalidades: Modalidades[];
    
    constructor(private http: Http, private router: Router, @Inject('BASE_URL') private baseUrl: string) {

        http.get(baseUrl + 'api/Admin/getModalidades').subscribe(result => {
            this.modalidades = result.json() as Modalidades[];
        }, error => console.error(error));
    }

    public getImagem(nome_m: String) {
        if (nome_m == 'Futebol') {
            return "https://image.flaticon.com/icons/svg/33/33736.svg";

        }
        else if (nome_m == 'Basquetebol') {
            return "https://image.flaticon.com/icons/svg/841/841740.svg";

        }
        else if (nome_m == 'Tenis') {
            return "https://image.flaticon.com/icons/svg/108/108649.svg";

        }
        else {
            return "https://image.flaticon.com/icons/svg/25/25251.svg";

        }

    }

    public RemoverModalidade(id: number) {
        
        let body = JSON.stringify({ nome: 'Ali' });

        let query = "id=" + id;

        this.http.post(this.baseUrl + 'api/Admin/RemoverModalidade?' + query, body).subscribe(
            data => {
                window.location.reload();
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }

    
}

interface Modalidades {
    id_m:number,
    nome: string
}