﻿import { Component, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Text } from '@angular/compiler';
import { Router } from "@angular/router";



@Component({
    selector: 'app-modalidade',
    templateUrl: './modalidade.component.html',
    styleUrls: ['./modalidade.component.css']
})
/** modalidade component*/
export class ModalidadeComponent {
    /** modalidade ctor */

    modalidade = "";

    constructor(private http: Http, private router: Router, @Inject('BASE_URL') private baseUrl: string) {

    }

    public AdicionarModalidade() {

        let body = JSON.stringify({ nome: 'Ali' });

        //let query = "nome=" + this.liga + "&" + "desporto=" + this.nome_m;
        let query = "nome=" + this.modalidade;

        this.http.post(this.baseUrl + 'api/Admin/InserirModalidades?' + query,body).subscribe(
            data => {
                this.router.navigate(['/admin/modalidades']);
            },
            error => {
                console.log(JSON.stringify(error.json()));
            }
        )

    }




    
}