﻿import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../shared/user.service';

@Component({
    selector: 'app-confirmaremail',
    templateUrl: './confirmaremail.component.html',
    styleUrls: ['./confirmaremail.component.css']
})
/** ComfirmarEmail component*/
export class ComfirmarEmailComponent {
    /** ComfirmarEmail ctor */

    successMessage: string = '';
    errorMessage: string = '';
    constructor(private route: ActivatedRoute, public userService: UserService) {
        this.route.queryParams.subscribe(params => {
            this.ComfirmarEmail(params['id']);
        });

    }
    ComfirmarEmail(id: any) {

        if (id != null)
            this.userService.ConfirmarEmail(id)
                .subscribe((data: any) => {
                    switch (data._body) {
                        case "sucesso": {
                            this.errorMessage = "";
                            this.successMessage = "Email confirmado com sucesso!!!";
                            break;
                        }
                        case "erro": {
                            this.successMessage = "";
                            this.errorMessage = "Ocorreu um erro!!!";
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                });

    }

}