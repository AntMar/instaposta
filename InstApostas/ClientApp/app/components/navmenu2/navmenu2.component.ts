﻿import { Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
    selector: 'nav-menu2',
    templateUrl: './navmenu2.component.html',
    styleUrls: ['./navmenu2.component.css']
})
export class NavMenu2Component {

    public userLogado: Utilizador;

    constructor(private route: ActivatedRoute, private router: Router, private http: Http, @Inject('BASE_URL') baseUrl: string) {
        http.get(baseUrl + 'api/User/CurrentUser').subscribe(result => {
            this.userLogado = result.json() as Utilizador;
        }, error => console.error(error));

        


    }
}

interface Utilizador {
    id: number;
}

