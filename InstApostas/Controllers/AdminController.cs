﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InstApostas.Models;

namespace InstApostas.Controllers
{

    [Route("api/[Controller]")]
    public class AdminController : Controller
    {
        private readonly InstApostaContext dbcontext;

        List<LigaAdpater> _ligas;
        List<ModalidadeAdapter> _modalidades;
        List<UsersAdapter> _users;
        List<DenunciaAdapter> _den_a;
        List<TotaisAdapter> tot;
        List<ApostaAdapter> _aposta;

        public AdminController(InstApostaContext context)
        {
            dbcontext = context;
        }

        //-----------------------------------------------------------------------------------------------------
        //-------------------------------------MÉTODOS GET-----------------------------------------------------

        [HttpGet("[action]")]
        public TotaisAdapter getTotais()
        {
            TotaisAdapter tot = new TotaisAdapter();
            tot.num_utilizadores = dbcontext.Utilizador.Count();
            tot.num_apostas = dbcontext.Aposta.Count();
            tot.den_total = dbcontext.Denuncia.Count();
            tot.den_abertas = dbcontext.Denuncia.Where(x => x.Estado == true).Count();
            tot.den_fechadas = dbcontext.Denuncia.Where(x => x.Estado == false).Count();

            return tot;
        }

        [HttpGet("[action]")]
        //public IEnumerable<ApostaAdapter> getApostas()
        //{
        //    _aposta = new List<ApostaAdapter>();
        //    string nome_ut = "";

        //    foreach (Aposta ap in dbcontext.Aposta)
        //    {
        //        nome_ut = dbcontext.Utilizador.Single(x => x.Id == ap.IdUtilizadorRegista).Nome;
        //        _aposta.Add(new ApostaAdapter(ap.Descricao, ap.DataInicio.ToString(), ap.DataFim.ToString(), ap.GrauConfianca, nome_ut, ap.OddTotal, Convert.ToBoolean(ap.Ganha)));
        //    }
        //    return _aposta;
        //}

        [HttpGet("[action]")]
        public IEnumerable<LigaAdpater> getLigas()
        {
            _ligas = new List<LigaAdpater>();

            foreach (Liga l in dbcontext.Liga) //para cada liga existente na base de dados
            {
                //vou buscar o nome da liga e o nome da modalidade
                _ligas.Add(new LigaAdpater(l.IdL, l.Nome, dbcontext.Modalidade.SingleOrDefault(x => x.IdM == l.IdModalidade).Nome));
            }
            return _ligas;
        }
        [HttpGet("[action]")]
        public LigaAdpater getLigaPorID(int id_liga)
        {
            Liga li = dbcontext.Liga.Single(x => x.IdL == id_liga);
            string nome_mod = "";
            nome_mod = dbcontext.Modalidade.SingleOrDefault(x => x.IdM == li.IdModalidade).Nome;
            return new LigaAdpater(li.IdL, li.Nome, nome_mod);
        }

        [HttpGet("[action]")]
        public IEnumerable<ModalidadeAdapter> getModalidades()
        {
            _modalidades = new List<ModalidadeAdapter>();

            foreach (Modalidade m in dbcontext.Modalidade)
            {
                _modalidades.Add(new ModalidadeAdapter(m.Nome, m.IdM));
            }
            return _modalidades;
        }

        [HttpGet("[action]")]
        public IEnumerable<UsersAdapter> getUsers()
        {
            _users = new List<UsersAdapter>();
            foreach (Utilizador u in dbcontext.Utilizador)
            {
                _users.Add(new UsersAdapter(u.Username, u.Nome, u.Email, u.Genero, Convert.ToInt32(u.Estado)));
            }
            return _users;
        }

        [HttpGet("[action]")]
        public IEnumerable<DenunciaAdapter> getDenuncia()
        {
            string denunciante = "";
            string denunciado = "";
            string conteudo = "";
            _den_a = new List<DenunciaAdapter>();

            foreach (Denuncia d in dbcontext.Denuncia)
            {

                if (d.TipoDenuncia == 0)
                {
                    conteudo = dbcontext.Utilizador.Single(x => x.Id == d.IdDenunciado).FotoPerfil;
                }
                else if (d.TipoDenuncia == 1)
                {
                    conteudo = dbcontext.Aposta.Single(x => x.Id == d.IdAposta).Foto;
                }
                else
                {
                    conteudo = dbcontext.Comentario.Single(x => x.Id == d.IdComentario).Descricao;
                }

                denunciante = dbcontext.Utilizador.SingleOrDefault(x => x.Id == d.IdDenunciante).Nome;
                denunciado = dbcontext.Utilizador.SingleOrDefault(x => x.Id == d.IdDenunciado).Nome;

                _den_a.Add(new DenunciaAdapter(d.IdDenuncia, d.IdComentario, d.IdAposta, denunciante, denunciado, d.IdDenunciante, d.IdDenunciado, d.TipoDenuncia, d.Data, d.Motivo, d.DataResposta, d.RespostaAdmin, d.Estado, conteudo));
            }
            return _den_a;
        }
        [HttpPost("[action]")]
        public DenunciaAdapter getDenunciabyID(int id_denuncia)

        {
            string denunciante = "";
            string denunciado = "";
            string conteudo = "";

            Denuncia d = dbcontext.Denuncia.Single(x => x.IdDenuncia == id_denuncia);
            if (d.TipoDenuncia == 0)
            {
                conteudo = dbcontext.Utilizador.Single(x => x.Id == d.IdDenunciado).FotoPerfil;
            }
            else if (d.TipoDenuncia == 1)
            {
                conteudo = dbcontext.Aposta.Single(x => x.Id == d.IdAposta).Foto;
            }
            else
            {
                conteudo = dbcontext.Comentario.Single(x => x.Id == d.IdComentario).Descricao;
            }

            denunciante = dbcontext.Utilizador.SingleOrDefault(x => x.Id == d.IdDenunciante).Nome;
            denunciado = dbcontext.Utilizador.SingleOrDefault(x => x.Id == d.IdDenunciado).Nome;

            return new DenunciaAdapter(d.IdDenuncia, d.IdComentario, d.IdAposta, denunciante, denunciado, d.IdDenunciante, d.IdDenunciado, d.TipoDenuncia, d.Data, d.Motivo, d.DataResposta, d.RespostaAdmin, d.Estado, conteudo);
        }

        //-----------------------------------------------------------------------------------------------
        //------------------------------------------MÉTODOS POST-----------------------------------------


        //neste método vai ser inserida uma Modalidade na base de dados.
        [HttpPost("[action]")]
        public StatusCodeResult InserirModalidades(String nome)
        {
            foreach (Modalidade Mod in dbcontext.Modalidade)
            {
                if (Mod.Nome == nome)
                {
                    //dar uma mensagem de erro
                    ModelState.AddModelError("", "Nao pode inserir duas modalidades com o mesmo nome");
                }
            }
            Modalidade m = new Modalidade();
            m.Nome = nome;
            m.Foto = "default.png";
            dbcontext.Modalidade.Add(m);
            dbcontext.SaveChanges();
            return StatusCode(201);
        }


        [HttpPost("[action]")]
        public StatusCodeResult RemoverModalidade(int id)
        {
            Modalidade M = dbcontext.Modalidade.Single(x => x.IdM == id);

            dbcontext.Liga.RemoveRange(dbcontext.Liga.Where(x => x.IdModalidade == M.IdM));
            dbcontext.SaveChanges();
            dbcontext.Modalidade.Remove(M);
            dbcontext.SaveChanges();

            return StatusCode(201);
        }

        [HttpPost("[action]")]
        public StatusCodeResult InserirLiga(String desporto, String nome)
        {
            //para verificar se existe algum já na bd
            foreach (Liga l in dbcontext.Liga)
            {
                if (l.Nome == nome)
                {
                    //MessageBox.Show("Some text", "Some title", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

            Liga L = new Liga();
            //o navigation permite navegar ao objeto modalidade e ir buscar o seu nome
            L.IdModalidade = dbcontext.Modalidade.SingleOrDefault(x => x.Nome == desporto).IdM;
            L.Nome = nome;
            dbcontext.Liga.Add(L);
            dbcontext.SaveChanges();

            return StatusCode(201);
        }

        [HttpPost("[action]")]
        public StatusCodeResult RemoverLiga(int id)
        {
            Liga l = dbcontext.Liga.Single(x => x.IdL == id);
            dbcontext.Liga.RemoveRange(l);
            dbcontext.SaveChanges();

            return StatusCode(201);
        }


        [HttpPost("[action]")]
        public StatusCodeResult EditarLiga(int id, String nome_editar)
        {

            Liga l = dbcontext.Liga.Single(x => x.IdL == id);
            l.Nome = nome_editar;
            dbcontext.SaveChanges();
            return StatusCode(201);
        }

        [HttpPost("[action]")]
        public StatusCodeResult ArquivarDenuncia(int id)
        {

            Denuncia d = dbcontext.Denuncia.Single(x => x.IdDenuncia == id);
            d.RespostaAdmin = "Denuncia arquivada";
            d.Estado = true;
            dbcontext.SaveChanges();

            return StatusCode(201);
        }


        [HttpPost("[action]")]
        public StatusCodeResult RespostaAdmin(String respostaAdmin, int id)
        {

            Denuncia d = dbcontext.Denuncia.Single(x => x.IdDenuncia == id);
            if (respostaAdmin == "" || respostaAdmin == "undefined")
                d.RespostaAdmin = "Sem resposta";
            else
                d.RespostaAdmin = respostaAdmin;
            d.Estado = true;
            dbcontext.SaveChanges();

            return StatusCode(201);
        }

        [HttpPost("[action]")]
        public StatusCodeResult AlterarEstadoUtilizador(String username)
        {
            Utilizador ul = dbcontext.Utilizador.Single(x => x.Username == username);
            if (ul.Estado == 0)
                ul.Estado = 2;
            else if (ul.Estado == 2)
                ul.Estado = 0;

            dbcontext.SaveChanges();
            return StatusCode(201);
        }

        [HttpPost("[action]")]
        public StatusCodeResult BloquearUtilizador(int id)
        {
            Utilizador ul = dbcontext.Utilizador.Single(x => x.Id == id);
            ul.Estado = 2;
            dbcontext.SaveChanges();
            return StatusCode(201);
        }


        //    //---------------------------------------------------------------
        //    //-------------------------CLASSES-------------------------------


        public class DenunciaAdapter
        {
            public int Id_denuncia { get; set; }
            public int? Id_comentario { get; set; }
            public int? Id_aposta { get; set; }
            public string denunciante { get; set; }
            public string denunciado { get; set; }
            public int Id_denunciante { get; set; }
            public int Id_denunciado { get; set; }
            public int Tipo_Denuncia { get; set; }
            public DateTime data { get; set; }
            public string motivo { get; set; }
            public DateTime? Data_Resposta { get; set; }
            public string resposta_administrador { get; set; }
            public bool estado { get; set; }
            public string conteudoDenunciado { get; set; }


            public DenunciaAdapter(int _id_denuncia, int? _id_comentario, int? _id_aposta, string _denunciante,
                string _denunciado, int _Id_denunciante, int _Id_denunciado, int _tipo_denuncia, DateTime _data, string _motivo, DateTime? _data_resposta, string _resposta_administrador, bool _estado, string _conteudoDenunciado)
            {
                this.Id_denuncia = _id_denuncia;
                this.Id_comentario = _id_comentario;
                this.Id_aposta = _id_aposta;
                this.denunciante = _denunciante;
                this.denunciado = _denunciado;
                this.Id_denunciado = _Id_denunciado;
                this.Id_denunciante = _Id_denunciante;
                this.Tipo_Denuncia = _tipo_denuncia;
                this.data = _data;
                this.motivo = _motivo;
                this.Data_Resposta = _data_resposta;
                this.resposta_administrador = _resposta_administrador;
                this.estado = _estado;
                this.conteudoDenunciado = _conteudoDenunciado;
            }
        }

        public class ApostaAdapter
        {
            public string descricao { get; set; }
            public string data_inicio { get; set; }
            public string data_fim { get; set; }
            public double grau_conf { get; set; }
            public string id_ut { get; set; }
            public double odd_total { get; set; }
            public bool ganha { get; set; }

            public ApostaAdapter(string _descricao, string _data_inicio, string _data_fim, double _grau_conf, string _id_ut, double _odd_total, bool _ganha)
            {
                this.descricao = _descricao;
                this.data_inicio = _data_inicio;
                this.data_fim = _data_fim;
                this.grau_conf = _grau_conf;
                this.id_ut = _id_ut;
                this.odd_total = _odd_total;
                this.ganha = _ganha;
            }
        }

        public class TotaisAdapter
        {
            public int den_abertas { get; set; }
            public int den_fechadas { get; set; }
            public int den_total { get; set; }
            public int num_utilizadores { get; set; }
            public int num_apostas { get; set; }
        }

        public class LigaAdpater
        {
            public int id_liga { get; set; }
            public string liga { get; set; }
            public string modalidade { get; set; }

            public LigaAdpater(int _id_liga, string _liga, string _modalidade)
            {
                this.liga = _liga;
                this.modalidade = _modalidade;
                this.id_liga = _id_liga;
            }
        }

        public class ModalidadeAdapter
        {
            public int id_m { get; set; }
            public string nome { get; set; }

            public ModalidadeAdapter(string _nome, int _id_m)
            {
                this.nome = _nome;
                this.id_m = _id_m;
            }
        }

        public class UsersAdapter
        {
            public string username { get; set; }
            public string nome { get; set; }
            public string email { get; set; }
            public string genero { get; set; }
            public int estado { get; set; }

            public UsersAdapter(string _username, string _nome, string _email, string _genero, int _estado)
            {
                this.username = _username;
                this.nome = _nome;
                this.email = _email;
                this.genero = _genero;
                this.estado = _estado;
            }
        }
    }
}