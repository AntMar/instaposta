﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InstApostas.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace InstApostas.Controllers
{
    [Produces("application/json")]
    [Route("api/Perfil")]
    public class PerfilController : Controller
    {
        private readonly InstApostaContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public PerfilController(InstApostaContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        // GET: api/Perfil
        [HttpGet]
        public IEnumerable<Utilizador> GetUtilizador()
        {
            return _context.Utilizador;
        }

        [Route("/api/Perfil/Util")]
        [HttpPost]
        public Utilizador GetUtilizador([FromBody] Utilizador u)
        {
            

            var utilizador =  _context.Utilizador.Where(m => m.Id == u.Id).FirstOrDefault();

           

            return utilizador;
        }

        [Route("/api/Perfil/Seguidores")]
        [HttpPost]
        public int GetSeguidores([FromBody] Utilizador u)
        {


            int seguidores = 0;
            foreach (SeguirUtilizador seg in _context.SeguirUtilizador.Where(m => m.IdUtilizadorSeguido == u.Id))
            {
                seguidores++;
            }

            return seguidores;
        }

        [Route("/api/Perfil/Aseguir")]
        [HttpPost]
        public int GetAseguir([FromBody] Utilizador u)
        {
            int aseguir = 0;
            foreach (SeguirUtilizador seg in _context.SeguirUtilizador.Where(m => m.IdUtilizadorSegue == u.Id))
            {
                aseguir++;
            }

            return aseguir;
        }

        [Route("/api/Perfil/Odd")]
        [HttpPost]
        public double OddMedia([FromBody] Utilizador u)
        {
            int apostas = 0;
            double odd = 0;
            foreach (Aposta a in _context.Aposta.Where(m => m.IdUtilizadorRegista == u.Id))
            {
                odd = odd + a.OddTotal;
                apostas++;
            }
            double oddmedia = (odd / apostas);
            if (apostas > 0)
            {
                oddmedia = Math.Round(oddmedia, 2, MidpointRounding.AwayFromZero);
                return oddmedia;
            }
            else
            {
                return 0;
            }
           
        }

        [Route("/api/Perfil/Avaliacao")]
        [HttpPost]
        public double Avaliacao([FromBody] Utilizador u)
        {
            int avaliacoes = 0;
            double stars = 0;
            foreach (AvaliaUtilizador a in _context.AvaliaUtilizador.Where(m => m.IdUtilizadorAvaliado == u.Id))
            {
                stars = stars + a.Classificacao;
                avaliacoes++;
            }
            double starsmedia = (stars / avaliacoes);
            if (avaliacoes > 0)
            {
                return starsmedia;
            }
            else
            {
                return 0;
            }

        }
       
        [Route("/api/Perfil/DenunciarUser")]
        [HttpPost]
        public string DenunciarUser([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            if (util != null)
            {
                Denuncia den = new Denuncia();
                den.Data = DateTime.Now;
                den.IdDenunciante = util.Id;
                den.IdDenunciado = model.idDenunciado;
                den.Motivo = model.motivo;
                den.TipoDenuncia = model.tipoDenuncia;
                _context.Denuncia.Add(den);
                _context.SaveChanges();
                return "sucesso";
            }
            else
            {
                return "erro";
            }
        }

        [Route("/api/Perfil/Foto")]
        [HttpPost]
        public string Upload()
        {
            var files = Request.Form.Files;

            foreach (var file in files)
            {
                //var diretoria = new DirectoryInfo(enviroment.WebRootPath("/Content/Fotos"));
                //var diretoria2 =
                //diretoria.CreateSubdirectory(Convert.ToString(Convert.ToInt32(dc.Imovel.Count() + 1)));
                //string caminho = HttpContext.Server.MapPath("/Content/Fotos") + "\\" + Convert.ToString(Convert.ToInt32(dc.Imovel.Count() + 1));
                //string fileName = Path.GetFileName(postedFile.FileName);
               
                string caminho = _hostingEnvironment.WebRootPath + "\\Content\\FotosUtil\\" + Convert.ToString(AuthController.user.Id) + ".png";
                
                using (var stream = new FileStream(caminho, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                string caminho2 = "../../Content/FotosUtil/" + Convert.ToString(AuthController.user.Id) + ".png";
                Utilizador util = _context.Utilizador.Single(x => x.Id == AuthController.user.Id);
                util.FotoPerfil = caminho2;
                _context.SaveChanges();
               
                //string contentRootPath = _hostingEnvironment.ContentRootPath;
            }

            return "esta bom";
        }

        [Route("/api/Perfil/Apostasa")]
        [HttpPost]
        public List<Aposta> GetApostasAtivas([FromBody] Utilizador u)
        {
            List<Aposta> ap = new List<Aposta>();

            foreach (Aposta a in _context.Aposta.Where(m => m.IdUtilizadorRegista == u.Id && m.DataFim > DateTime.Now))           
            {
                ap.Add(a);
            }

            return ap;
        }

        [Route("/api/Perfil/Apostash")]
        [HttpPost]
        public List<Aposta> GetApostasHistorico([FromBody] Utilizador u)
        {
          
            List<Aposta> ap = new List<Aposta>();

            foreach (Aposta a in _context.Aposta.Where(m => m.IdUtilizadorRegista == u.Id && m.DataFim < DateTime.Now))
            {
                ap.Add(a);
            }

            return ap;
        }

        [Route("/api/Perfil/ElimAposta")]
        [HttpPost]
        public string EliminarAposta([FromBody] Aposta aposta)
        {
           
            foreach (Comentario c in _context.Comentario.Where(m => m.IdAposta == aposta.Id ))
            {
                _context.Comentario.Remove(c);
                
            }
            
            foreach (Avaliacao a in _context.Avaliacao.Where(m => m.IdAposta == aposta.Id))
            {
                _context.Avaliacao.Remove(a);
                
            }

            foreach (Denuncia d in _context.Denuncia.Where(m => m.IdAposta == aposta.Id))
            {
                _context.Denuncia.Remove(d);
                
            }

            foreach (LigaAposta l in _context.LigaAposta.Where(m => m.IdAposta == aposta.Id))
            {
                _context.LigaAposta.Remove(l);
               
            }
            Aposta ap = new Aposta();
            ap = _context.Aposta.Where(X => X.Id == aposta.Id).FirstOrDefault();
            
            _context.Aposta.Remove(ap);
            _context.SaveChanges();

            return "true";
        }



        [Route("/api/Perfil/SeguirUtil")]
        [HttpPost]
        public string SeguirUtil([FromBody] Utilizador u)
        {
            Utilizador a = new Utilizador();
            a = AuthController.user;

            SeguirUtilizador segU = new SeguirUtilizador();
            segU = _context.SeguirUtilizador.Where(X => X.IdUtilizadorSeguido == u.Id && X.IdUtilizadorSegue ==a.Id).FirstOrDefault();
            if (segU == null)
            {
                SeguirUtilizador seg = new SeguirUtilizador();
                seg.IdUtilizadorSegue = a.Id;
                seg.IdUtilizadorSeguido = u.Id;

                _context.SeguirUtilizador.Add(seg);
                _context.SaveChanges();

                return "segue";
            }
            else {
                _context.SeguirUtilizador.Remove(segU);
                _context.SaveChanges();
                return "naosegue";
            }           
        }

        [Route("/api/Perfil/Versesegue")]
        [HttpPost]
        public string Versesegue([FromBody] Utilizador u)
        {
            Utilizador a = new Utilizador();
            a = AuthController.user;

            SeguirUtilizador segU = new SeguirUtilizador();
            segU = _context.SeguirUtilizador.Where(X => X.IdUtilizadorSeguido == u.Id && X.IdUtilizadorSegue == a.Id).FirstOrDefault();
            if (segU == null)
            {               
                return "naosegue";
            }
            else
            {              
                return "segue";
            }
        }

        [Route("/api/Perfil/Editar")]
        [HttpPost]
        public string EditarPerfil([FromBody] dynamic model)
        {           
            Utilizador util = new Utilizador();
            int Id =  Int32.Parse(model.Id.Value);
            util = _context.Utilizador.Where(X => X.Id == Id).FirstOrDefault();

            if (util != null)
            {
                if (model.nome != "") { 
                    util.Nome = model.nome;                   
                }

                if (model.UserName != "")
                {
                    util.Username = model.UserName;
                }

                if (model.Email != "")
                {
                    string Email = model.Email;
                    if (util.Email!= Email) {                         
                        Utilizador a = _context.Utilizador.Where(x => x.Email == Email).FirstOrDefault();
                        if (a == null)
                        {
                            util.Email = model.Email;
                        }
                        else
                        {
                            return "mail";
                        }
                    }

                }

                if (model.Password != "")
                {
                   
                    byte[] data = System.Text.Encoding.ASCII.GetBytes(model.Password.Value);
                    data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
                    String hash = System.Text.Encoding.ASCII.GetString(data);
                    model.Password = hash;
                    if(model.Password == util.Password)
                    {
                        if(model.Password1 == model.Password2)
                        {
                            byte[] data1 = System.Text.Encoding.ASCII.GetBytes(model.Password1.Value);
                            data1 = new System.Security.Cryptography.SHA256Managed().ComputeHash(data1);
                            String hash1 = System.Text.Encoding.ASCII.GetString(data1);
                            model.Password1 = hash1;
                            util.Password = model.Password1;
                        }
                        else
                        {
                            return "password2";
                        }
                    }
                    else
                    {
                        return "password";
                    }
                }

                if (model.modalidadefavorita != "")
                {
                    util.ModalidadeFavorita = model.modalidadefavorita;
                }
                
                _context.SaveChanges();
                return "sucesso";
            }
            return "erro";
        }

        [Route("/api/Perfil/Stars")]
        [HttpPost]
        public string Stars([FromBody] dynamic model)
        {
            Utilizador a = new Utilizador();
            a = AuthController.user;

            int Id = model.Id;

            AvaliaUtilizador au = new AvaliaUtilizador();
            au = _context.AvaliaUtilizador.Where(X => X.IdUtilizadorAvaliador == a.Id && X.IdUtilizadorAvaliado == Id).FirstOrDefault();
            if (au == null)
            {
                AvaliaUtilizador b = new AvaliaUtilizador();
                b.IdUtilizadorAvaliado = Id;
                b.IdUtilizadorAvaliador = a.Id;
                b.Classificacao = model.stars;
                _context.AvaliaUtilizador.Add(b);
                _context.SaveChanges();
                return "ok";
            }
            else
            {
                au.Classificacao = model.stars;
                _context.SaveChanges();
                return "ok";
            }
        }

        [Route("/api/Perfil/VerStars")]
        [HttpPost]
        public string VerStars([FromBody] dynamic model)
        {
            Utilizador a = new Utilizador();
            a = AuthController.user;

            int Id = model.Id;

            AvaliaUtilizador au = new AvaliaUtilizador();
            au = _context.AvaliaUtilizador.Where(X => X.IdUtilizadorAvaliador == a.Id && X.IdUtilizadorAvaliado == Id).FirstOrDefault();
            if (au == null)
            {
                return "0";
            }
            else
            {
                string stars = au.Classificacao.ToString();
                
                return stars;
            }
        }

        // PUT: api/Perfil/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUtilizador([FromRoute] int id, [FromBody] Utilizador utilizador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != utilizador.Id)
            {
                return BadRequest();
            }

            _context.Entry(utilizador).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UtilizadorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Perfil
        [HttpPost]
        public async Task<IActionResult> PostUtilizador([FromBody] Utilizador utilizador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Utilizador.Add(utilizador);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUtilizador", new { id = utilizador.Id }, utilizador);
        }

        // DELETE: api/Perfil/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUtilizador([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var utilizador = await _context.Utilizador.SingleOrDefaultAsync(m => m.Id == id);
            if (utilizador == null)
            {
                return NotFound();
            }

            _context.Utilizador.Remove(utilizador);
            await _context.SaveChangesAsync();

            return Ok(utilizador);
        }

        private bool UtilizadorExists(int id)
        {
            return _context.Utilizador.Any(e => e.Id == id);
        }
    }
}