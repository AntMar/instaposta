﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InstApostas.Models;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Hosting;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace InstApostas.Controllers
{
    //[Route("api/[controller]")]
    public class ApostasController : Controller
    {
        private readonly InstApostaContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ApostasController(InstApostaContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;

        }

       

        [Route("api/Aposta/Liga")]
        [HttpGet]
        public IEnumerable<Liga> GetLiga()
        {
            List<Liga> mod = new List<Liga>();
            mod = _context.Liga.ToList();
            return mod;
        }

        [Route("api/Aposta/Modalidades")]
        [HttpGet]
        public IEnumerable<Modalidade> GetModalidades()
        {
            List<Modalidade> mod = new List<Modalidade>();
            mod = _context.Modalidade.ToList();
            return mod;
        }

        

        [Route("api/Aposta/InserirAposta")]
        [HttpPost]
        public string InserirAposta([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            if (!(Convert.ToDateTime(model.dataInicio.Value) > Convert.ToDateTime(model.dataFim.Value)))
            {
                
                Aposta aposta = new Aposta();
                aposta.IdTipoAposta = Convert.ToInt32(Convert.ToString(model.tipoAposa));
                aposta.IdUtilizadorRegista = Convert.ToInt32(util.Id);
                aposta.OddTotal = Convert.ToDouble(Convert.ToString(model.odd));
                aposta.GrauConfianca = Convert.ToDouble(Convert.ToString(model.risco));
                aposta.Descricao = Convert.ToString(Convert.ToString(model.descricao));
                aposta.DataInicio = Convert.ToDateTime(model.dataInicio.Value);
                aposta.DataFim = Convert.ToDateTime(model.dataFim.Value);
                aposta.Ganha = false;
                aposta.Foto = "def";
                _context.Aposta.Add(aposta);
                _context.SaveChanges();
                string caminho = "../../Content/Apostas/" + Convert.ToString(aposta.Id) + ".png";
                aposta.Foto = caminho;
                _context.SaveChanges();
                LigaAposta ap = new LigaAposta();
                //pode haver problemas
                ap.IdAposta = aposta.Id;
                ap.IdLiga = model.liga;
                _context.LigaAposta.Add(ap);
                _context.SaveChanges();

                return "sucesso";


            }
            else
            {
                return "data";
            }
        }

        //[HttpPost]
        //[Route("api/Apostas/Foto")]
        //public Task Foto(IFormFile file)
        //{
        //    if (file == null) throw new Exception("File is null");
        //    if (file.Length == 0) throw new Exception("File is empty");

        //    using (Stream stream = file.OpenReadStream())
        //    {
        //        using (var binaryReader = new BinaryReader(stream))
        //        {
        //            var fileContent = binaryReader.ReadBytes((int)file.Length);
        //            await _uploadService.AddFile(fileContent, file.FileName, file.ContentType);
        //        }
        //    }
        //}

        

        [Route("api/Apostas/Foto")]
        [HttpPost]
        public string Upload()
        {
            var files = Request.Form.Files;
            
            foreach (var file in files)
            {
                //var diretoria = new DirectoryInfo(enviroment.WebRootPath("/Content/Fotos"));
                //var diretoria2 =
                //diretoria.CreateSubdirectory(Convert.ToString(Convert.ToInt32(dc.Imovel.Count() + 1)));
                //string caminho = HttpContext.Server.MapPath("/Content/Fotos") + "\\" + Convert.ToString(Convert.ToInt32(dc.Imovel.Count() + 1));
                //string fileName = Path.GetFileName(postedFile.FileName);
                Aposta ap = _context.Aposta.Last();
                string caminho = _hostingEnvironment.WebRootPath + "\\Content\\Apostas\\" + Convert.ToString(ap.Id) + ".png";
                using (var stream = new FileStream(caminho, FileMode.Create))
                {
                    file.CopyTo(stream);
                }
                
                //string contentRootPath = _hostingEnvironment.ContentRootPath;
            }

            return "esta bom";
        }


        [Route("api/Apostas/TotalLikes")]
        [HttpPost]
        public int TotalLikes([FromBody] dynamic model)
        {
            List<Avaliacao> ava = new List<Avaliacao>();
            int id = model.Id;
            foreach (Avaliacao i in _context.Avaliacao.Where(x => x.IdAposta == id && x.Estado == 1))
            {
                ava.Add(i);
            }

            return ava.Count();
        }

        [Route("api/Aposta/guardarLike")]
        [HttpPost]
        public string guardarLike([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            if (util!=null)
            {
                Avaliacao ava = new Avaliacao();
                ava.IdAposta = model.idAposta;
                ava.IdUtilizador = util.Id;
                ava.Estado = 1;
                _context.Avaliacao.Add(ava);
                _context.SaveChanges();
                return "sucesso";
            }
            else
            {
                return "erro";
            }
        }

        [Route("api/Aposta/verificarLike")]
        [HttpPost]
        public string verificarLike([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            int idAposta = model.idAposta;
            if (_context.Avaliacao.Any(x=> x.IdAposta == idAposta && x.IdUtilizador == util.Id && x.Estado == 1))
            {
                return "sucesso";
            }
            else
            {
                return "erro";
            }
        }

        [Route("api/Aposta/carregarLike")]
        [HttpPost]
        public string carregarLike([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            int idAposta = model.idAposta;
            if (util != null)
            {
                Avaliacao ava = _context.Avaliacao.FirstOrDefault(x => x.IdAposta == idAposta && x.IdUtilizador == util.Id);
                if (ava==null)
                {
                    Avaliacao a = new Avaliacao();
                    a.IdAposta = idAposta;
                    a.IdUtilizador = util.Id;
                    a.Estado = 1;
                    a.Data = DateTime.Now.Date;
                    _context.Avaliacao.Add(a);
                    _context.SaveChanges();
                    return "sucesso";
                }
                else
                {
                    if (ava.Estado == 1)
                        ava.Estado = 0;
                    else
                        ava.Estado = 1;
                    _context.SaveChanges();
                    return "sucesso";
                }
            }
            else
            {
                return "erro";
            }
        }

        [Route("api/Aposta/VisualizarAposta")]
        [HttpPost]
        public Aposta VisualizarAposta([FromBody] dynamic model)
        {

            int idAposta = Convert.ToInt32(model.Id.Value);
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);

            return aposta;
        }

        [Route("api/Aposta/CarregarComentarios")]
        [HttpPost]
        public IEnumerable<Comentario> CarregarComentarios([FromBody] dynamic model)
        {
            List<Comentario> com = new List<Comentario>();
            int idAposta = Convert.ToInt32(model.Id.Value);
            foreach (Comentario c in _context.Comentario.Where(x=> x.IdAposta == idAposta))
            {
                com.Add(c);
            }

            com.Reverse();
            return com;
        }

       
        [Route("api/Aposta/InserirComentario")]
        [HttpPost]
        public string InserirComentario([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            if (util != null)
            {
                Comentario com = new Comentario();
                com.IdAposta = model.idAposta;
                com.Descricao = model.comentario.Value;
                com.IdUtilizador = util.Id;
                com.Data = DateTime.Now;
                _context.Comentario.Add(com);
                _context.SaveChanges();
                return "sucesso";
            }
            else
            {
                return "erro";
            }
        }

        

        [Route("api/Aposta/inserirDenunciaAposta")]
        [HttpPost]
        public string InserirDenunciaAposta([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            if (util != null)
            {
                Denuncia den = new Denuncia();
                den.Data = DateTime.Now;
                den.IdAposta = model.idAposta;
                den.IdDenunciante = util.Id;
                den.IdDenunciado = model.idDenunciado;
                den.Motivo = model.motivo;
                den.TipoDenuncia = model.tipoDenuncia;
                _context.Denuncia.Add(den);
                _context.SaveChanges();
                return "sucesso";
            }
            else
            {
                return "erro";
            }
        }

        [Route("api/Aposta/inserirDenunciaComentario")]
        [HttpPost]
        public string InserirDenunciaComentario([FromBody] dynamic model)
        {
            Utilizador util = new Utilizador();
            util = AuthController.user;
            if (util != null)
            {
                Denuncia den = new Denuncia();
                den.Data = DateTime.Now;
                den.IdComentario = model.idComentario;
                den.IdDenunciante = util.Id;
                den.IdDenunciado = model.idDenunciado;
                den.Motivo = model.motivo;
                den.TipoDenuncia = model.tipoDenuncia;
                _context.Denuncia.Add(den);
                _context.SaveChanges();
                return "sucesso";
            }
            else
            {
                return "erro";
            }
        }

        [Route("api/Aposta/DonoApostaId")]
        [HttpPost]
        public int DonoApostaId([FromBody] dynamic model)
        {

            int idAposta = model.Id;
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);

            Utilizador util = _context.Utilizador.Single(x=> x.Id == aposta.IdUtilizadorRegista);

            return util.Id;
        }

        [Route("api/Aposta/DonoApostaUsername")]
        [HttpPost]
        public string DonoApostaUsername([FromBody] dynamic model)
        {

            int idAposta = model.Id;
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);

            Utilizador util = _context.Utilizador.Single(x => x.Id == aposta.IdUtilizadorRegista);

            return util.Username;
        }

        [Route("api/Aposta/CarregarTipoAposta")]
        [HttpPost]
        public string CarregarTipoAposta([FromBody] Aposta model)
        {

            int idAposta = model.Id;
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);

            TipoAposta tipo = _context.TipoAposta.Single(x => x.Id == aposta.IdTipoAposta);
            return tipo.Descricao;
        }

        [Route("api/Aposta/CarregarModalidade")]
        [HttpPost]
        public string CarregarModalidade([FromBody] Aposta model)
        {

            int idAposta = model.Id;
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);

            LigaAposta liga = _context.LigaAposta.Single(x => x.IdAposta == aposta.Id);
            Liga l = _context.Liga.Single(x => x.IdL == liga.IdLiga);
            Modalidade mod = _context.Modalidade.Single(x=> x.IdM == l.IdModalidade);

            return mod.Nome;
        }

        [Route("api/Aposta/CarregarLiga")]
        [HttpPost]
        public string CarregarLiga([FromBody] Aposta model)
        {

            int idAposta = model.Id;
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);

            LigaAposta liga = _context.LigaAposta.Single(x => x.IdAposta == aposta.Id);
            Liga l = _context.Liga.Single(x => x.IdL == liga.IdLiga);
            return l.Nome;
        }

        [Route("api/Aposta/CarregarDescricao")]
        [HttpPost]
        public string CarregarDescricao([FromBody] Aposta model)
        {

            int idAposta = Convert.ToInt32(model.Id);
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);
            return aposta.Descricao;
        }

        [Route("api/Aposta/CarregarOdd")]
        [HttpPost]
        public double CarregarOdd([FromBody] Aposta model)
        {

            int idAposta = Convert.ToInt32(model.Id);
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);
            return aposta.OddTotal;
        }

        [Route("api/Aposta/CarregarRisco")]
        [HttpPost]
        public double CarregarRisco([FromBody] Aposta model)
        {

            int idAposta = Convert.ToInt32(model.Id);
            Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);
            return aposta.GrauConfianca;
        }

        //[Route("api/Aposta/VisualizarAposta")]
        //[HttpPost]
        //public Aposta CarregarAposta([FromBody] Aposta model)
        //{

        //    int idAposta = Convert.ToInt32(model.Id);
        //    Aposta aposta = _context.Aposta.Single(x => x.Id == idAposta);
        //    return aposta;
        //}

        // GET: api/<controller>
        [HttpGet("[action]")]
        public IEnumerable<ApostasInfoDTO> GetApostasInfo()
        {
            var lista = new List<ApostasInfoDTO>();

            lista.Add(new ApostasInfoDTO() { nome = "aposta 1", foto = "aposta.jpg" });
            lista.Add(new ApostasInfoDTO() { nome = "aposta 2", foto = "aposta.jpg" });
            lista.Add(new ApostasInfoDTO() { nome = "aposta 3", foto = "aposta.jpg" });

            var lista2 = _context.Aposta
                                .Where(Z=> Z.DataInicio >= DateTime.Now.AddDays(-3))
                                .Select(
                                        X => new ApostasInfoDTO()
                                        { nome = X.Descricao, foto = X.Foto })
                                .ToList();


            //lista.AddRange(lista2);


            return lista;
        }
        
    }

  


    public class ApostasInfoDTO {
        public String nome { get; set; }
        public String foto { get; set; }
    }
}
