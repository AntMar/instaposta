﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InstApostas.Controllers
{
    [Route("api/[controller]")]
    public class NotificacoesController : Controller
    {
        public class Notificacoes
        {
            //variaveis gerais
            public string DestUser { get; set; }//utilizador a quem a notificacao pertence
            public string SourceUser { get; set; }//utilizador que provocou a notificacao
            //especificas
            public string Url { get; set; }
            public int ComentarioID { get; set; }// id do comentario comentado
            public string Tipo { get; set; }//1 foto, 2 comentario, 3 seguir

            public Notificacoes()
            {
                
            }
        }


        [HttpGet("[action]")]
        public IEnumerable<Notificacoes> Notifica()
        {
            var rng = new Random();
            return Enumerable.Range(1, 10).Select(index => new Notificacoes
            {
                SourceUser = "Utilizador" + Convert.ToString(index),
                Url = "link_"+ rng.Next(1,100),
                Tipo = Convert.ToString(rng.Next(1,4))

            });
        }

    }
}