﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using InstApostas.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace InstApostas.Controllers
{

    public class AuthController : Controller
    {
        private readonly InstApostaContext _context;
        public static Utilizador user = new Utilizador();

        public AuthController(InstApostaContext context)
        {
            _context = context;
        }

        [Route("api/User/CurrentUser")]
        [HttpGet]
        public Utilizador GetCurrentUSer()
        {
            return user;
        }

        [Route("api/User/Modalidades")]
        [HttpGet]
        public IEnumerable<Modalidade> GetModalidades()
        {
            List<Modalidade> mod = new List<Modalidade>();
            mod = _context.Modalidade.ToList();
            return mod;
        }
        [Route("api/User/Register")]
        [HttpPost]
        public async Task<string> RegisterAsync([FromBody] Utilizador model)
        {
            Utilizador util = new Utilizador();
            util = _context.Utilizador.Where(X => X.Email == model.Email).FirstOrDefault();
            if (util == null)
            {
                util = _context.Utilizador.Where(X => X.Username == model.Username).FirstOrDefault();
                if (util == null)
                {
                    if (model.DataNascimento.Value.Year > 2000)
                    {
                        return "idade";
                    }
                    else
                    {
                        Utilizador user = new Utilizador();
                        user.DataNascimento = model.DataNascimento;
                        user.Email = model.Email;

                        byte[] data = System.Text.Encoding.ASCII.GetBytes(model.Password);
                        data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
                        String hash = System.Text.Encoding.ASCII.GetString(data);
                        
                        user.Password = hash;
                        user.Username = model.Username;
                        user.FotoPerfil = "../../img/utilizador.png";
                        user.ModalidadeFavorita = model.ModalidadeFavorita;
                        user.Nome = model.Nome;
                        user.Genero = model.Genero;
                        user.Provider = "";
                        user.Estado = 3;
                        user.IdFacebook = "";
                        user.IdGoogle = "";
                        user.DataRegisto = DateTime.Now;
                        
                        _context.Utilizador.Add(user);
                        _context.SaveChanges();

                        await SendemailAsync(user.Email, "Confirmação de e-mail", "Para confirmar o seu e-mail clique no seguinte link: https://localhost:44346/ConfirmarEmail?id=" + util.Id);

                        return "sucesso";


                    }
                }
                else
                {
                    return "erro";
                }

            }
            else
            {
                return "erro";
            }
        }
        [Route("api/User/Login")]
        [HttpPost]
        public string Login([FromBody] Utilizador model)
        {
            Utilizador util = new Utilizador();
            util = _context.Utilizador.Where(X => X.Email == model.Email).FirstOrDefault();

            if (util != null)
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes(model.Password);
                data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
                String hash = System.Text.Encoding.ASCII.GetString(data);
                model.Password = hash;

                if (model.Password == util.Password)
                {
                    if (util.Estado == 2)
                    {
                        return "bloqueado";
                    }
                    user = util;
                    util.IsLogged = true;
                    _context.SaveChanges();
                    return "sucesso";
                }
                else
                {
                    return "pass";
                }
            }
            else
            {
                if(model.Email== "admin@admin.com" && model.Password == "admin")
                {
                    return "admin";
                }
                return "erro";
            }
            

        }

        [Route("api/User/Resetpass")]
        [HttpPost]
        public async Task<string> ResetpasAsync([FromBody] Utilizador model)
        {


            Utilizador util = new Utilizador();
            util = _context.Utilizador.Where(X => X.Email == model.Email).FirstOrDefault();
            if (util == null)
            {
                return "erro";
            }
            if (util.Provider != "facebook" && util.Provider != "google")
            {
                Random rnd = new Random();
                int pass = rnd.Next(345678, 234567896);
                string pas = pass.ToString();

                byte[] data = System.Text.Encoding.ASCII.GetBytes(pas);
                data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
                String hash = System.Text.Encoding.ASCII.GetString(data);

                util.Password = hash;
                _context.SaveChanges();

                await SendemailAsync(model.Email, "Recuperação da password", "A sua nova password é:" + pas);

                return "sucesso";
            }
            else
            {
                if (util.Provider == "facebook")
                {
                    return "facebook";
                }
                else
                {
                    return "google";
                }
            }
        }

        [Route("/api/User/SocialLogin")]
        [HttpPost]
        public string SocialLogin([FromBody] Utilizador model)
        {
            Utilizador util = new Utilizador();
            util = _context.Utilizador.Where(X => X.Email == model.Email).FirstOrDefault();
            if (util == null)
            {
                return "erro";
            }
            if (model.Email.ToLower() == util.Email.ToLower())
            {
                if (util.Provider == "facebook" || util.Provider == "google")
                {
                    if (util.Estado == 2)
                    {
                        return "bloqueado";
                    }
                    user = util;
                    util.IsLogged = true;
                    _context.SaveChanges();
                    return "sucesso";
                }
                else
                {
                    return "registado";
                }
            }

            return "erro";

        }
        [Route("/api/User/SocialRegister")]
        [HttpPost]
        public async Task<string> SocialRegisterAsync([FromBody] Utilizador model)
        {

            Utilizador util = new Utilizador();
            util = _context.Utilizador.Where(X => X.Email == model.Email).FirstOrDefault();

            if (util != null)
            {
                return "erro";

            }



            Utilizador user = new Utilizador();
            user.DataNascimento = Convert.ToDateTime("01/01/2000 00:00:00");
            user.Email = model.Email;
            user.Password = "";
            user.Username = model.Nome + DateTime.Now.ToString("yyyyMMddhhmmss");
            user.Nome = model.Nome;
            user.FotoPerfil = model.FotoPerfil;
            user.ModalidadeFavorita = model.ModalidadeFavorita;
            user.Username = model.Nome;
            user.Genero = "";
            user.Provider = model.Provider;
            user.Estado = 3;
            user.IdGoogle = model.IdGoogle;
            user.IdFacebook = model.IdFacebook;

            _context.Utilizador.Add(user);
            _context.SaveChanges();
            await SendemailAsync(user.Email, "Confirmação de e-mail", "Para confirmar o seu e-mail clique no seguinte link: https://localhost:44346/ConfirmarEmail?id=" + util.Id);

            return "sucesso";


        }
        [Route("api/User/Logout")]
        [HttpGet]
        public Boolean Logout()
        {
            Utilizador util = new Utilizador();
            util = _context.Utilizador.Where(X => X.IsLogged == true).FirstOrDefault();

            if (util != null)
            {

                util.IsLogged = false;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        [Route("/api/User/ConfirmarEmail")]
        [HttpPost]
        public string ConfirmarEmail([FromBody] Utilizador model)
        {
            int id;
            id=Convert.ToInt32(model.Id);
            Utilizador util = new Utilizador();
            util = _context.Utilizador.Where(X => X.Id == id).FirstOrDefault();

            if (util != null)
            {

                util.Estado = 0;
                _context.SaveChanges();
                return "sucesso";
            }
            return "erro";
        }
        public async Task SendemailAsync(string email, string subject, string mensagem)
        {
            //https://myaccount.google.com/lesssecureapps
            var body = "<p>Email From: {0} ({1})</p><p>Message:</p><p>{2}</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress(email));  // replace with valid value 
            message.From = new MailAddress("InstApostasMp5@gmail.com");  // replace with valid value
            message.Subject = subject;
            message.Body = string.Format(body, "InstApostas", "InstApostasMp5@gmail.com", mensagem/*+User.id*/);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = "InstApostasMp5@gmail.com",  // replace with valid value
                    Password = "#Mp5Tp3#"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);

            }
            return;
        }
    }
}