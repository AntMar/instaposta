﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Modalidade
    {
        public Modalidade()
        {
            Liga = new HashSet<Liga>();
            Utilizador = new HashSet<Utilizador>();
        }

        public int IdM { get; set; }
        public string Nome { get; set; }
        public string Foto { get; set; }

        public ICollection<Liga> Liga { get; set; }
        public ICollection<Utilizador> Utilizador { get; set; }
    }
}
