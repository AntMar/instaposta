﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Liga
    {
        public Liga()
        {
            LigaAposta = new HashSet<LigaAposta>();
        }

        public int IdL { get; set; }
        public int IdModalidade { get; set; }
        public string Nome { get; set; }

        public Modalidade IdModalidadeNavigation { get; set; }
        public ICollection<LigaAposta> LigaAposta { get; set; }
    }
}
