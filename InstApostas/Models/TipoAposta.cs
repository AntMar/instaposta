﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class TipoAposta
    {
        public TipoAposta()
        {
            Aposta = new HashSet<Aposta>();
        }

        public int Id { get; set; }
        public string Descricao { get; set; }

        public ICollection<Aposta> Aposta { get; set; }
    }
}
