﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Faqs
    {
        public int IdFaq { get; set; }
        public string TituloFaq { get; set; }
        public string DescriçaoFaq { get; set; }
        public DateTime Data { get; set; }
        public bool? Estado { get; set; }
    }
}
