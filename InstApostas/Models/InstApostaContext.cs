﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace InstApostas.Models
{
    public partial class InstApostaContext : DbContext
    {
        public virtual DbSet<Administrador> Administrador { get; set; }
        public virtual DbSet<Aposta> Aposta { get; set; }
        public virtual DbSet<Avaliacao> Avaliacao { get; set; }
        public virtual DbSet<AvaliaUtilizador> AvaliaUtilizador { get; set; }
        public virtual DbSet<Comentario> Comentario { get; set; }
        public virtual DbSet<Denuncia> Denuncia { get; set; }
        public virtual DbSet<Faqs> Faqs { get; set; }
        public virtual DbSet<Liga> Liga { get; set; }
        public virtual DbSet<LigaAposta> LigaAposta { get; set; }
        public virtual DbSet<Modalidade> Modalidade { get; set; }
        public virtual DbSet<SeguirUtilizador> SeguirUtilizador { get; set; }
        public virtual DbSet<TipoAposta> TipoAposta { get; set; }
        public virtual DbSet<Utilizador> Utilizador { get; set; }


        public InstApostaContext(DbContextOptions db)
            :base(db)
        {

        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Administrador>(entity =>
            {
                entity.HasKey(e => e.IdAdmin);

                entity.HasIndex(e => e.Email)
                    .HasName("UQ__Administ__A9D1053435BCFE0A")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("UQ__Administ__536C85E438996AB5")
                    .IsUnique();

                entity.Property(e => e.IdAdmin).HasColumnName("ID_Admin");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Aposta>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DataFim)
                    .HasColumnName("Data_Fim")
                    .HasColumnType("date");

                entity.Property(e => e.DataInicio)
                    .HasColumnName("Data_Inicio")
                    .HasColumnType("date");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.GrauConfianca).HasColumnName("Grau_Confianca");

                entity.Property(e => e.IdTipoAposta).HasColumnName("ID_TipoAposta");

                entity.Property(e => e.IdUtilizadorRegista).HasColumnName("ID_Utilizador_Regista");

                entity.Property(e => e.OddTotal).HasColumnName("Odd_Total");

                entity.HasOne(d => d.IdTipoApostaNavigation)
                    .WithMany(p => p.Aposta)
                    .HasForeignKey(d => d.IdTipoAposta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Aposta__ID_TipoA__0C85DE4D");

                entity.HasOne(d => d.IdUtilizadorRegistaNavigation)
                    .WithMany(p => p.Aposta)
                    .HasForeignKey(d => d.IdUtilizadorRegista)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Aposta__ID_Utili__0B91BA14");
            });

            modelBuilder.Entity<Avaliacao>(entity =>
            {
                entity.HasKey(e => new { e.IdUtilizador, e.IdAposta });

                entity.Property(e => e.IdUtilizador).HasColumnName("ID_Utilizador");

                entity.Property(e => e.IdAposta).HasColumnName("ID_Aposta");

                entity.Property(e => e.Data).HasColumnType("date");

                entity.HasOne(d => d.IdApostaNavigation)
                    .WithMany(p => p.Avaliacao)
                    .HasForeignKey(d => d.IdAposta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Avaliacao__ID_Ap__17F790F9");

                entity.HasOne(d => d.IdUtilizadorNavigation)
                    .WithMany(p => p.Avaliacao)
                    .HasForeignKey(d => d.IdUtilizador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Avaliacao__ID_Ut__17036CC0");
            });

            modelBuilder.Entity<AvaliaUtilizador>(entity =>
            {
                entity.HasKey(e => new { e.IdUtilizadorAvaliador, e.IdUtilizadorAvaliado });

                entity.Property(e => e.IdUtilizadorAvaliador).HasColumnName("ID_Utilizador_Avaliador");

                entity.Property(e => e.IdUtilizadorAvaliado).HasColumnName("ID_Utilizador_Avaliado");

                entity.HasOne(d => d.IdUtilizadorAvaliadoNavigation)
                    .WithMany(p => p.AvaliaUtilizadorIdUtilizadorAvaliadoNavigation)
                    .HasForeignKey(d => d.IdUtilizadorAvaliado)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AvaliaUti__ID_Ut__02FC7413");

                entity.HasOne(d => d.IdUtilizadorAvaliadorNavigation)
                    .WithMany(p => p.AvaliaUtilizadorIdUtilizadorAvaliadorNavigation)
                    .HasForeignKey(d => d.IdUtilizadorAvaliador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__AvaliaUti__ID_Ut__02084FDA");
            });

            modelBuilder.Entity<Comentario>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Data).HasColumnType("date");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IdAposta).HasColumnName("ID_Aposta");

                entity.Property(e => e.IdUtilizador).HasColumnName("ID_Utilizador");

                entity.HasOne(d => d.IdApostaNavigation)
                    .WithMany(p => p.Comentario)
                    .HasForeignKey(d => d.IdAposta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Comentari__ID_Ap__123EB7A3");

                entity.HasOne(d => d.IdUtilizadorNavigation)
                    .WithMany(p => p.Comentario)
                    .HasForeignKey(d => d.IdUtilizador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Comentari__ID_Ut__114A936A");
            });

            modelBuilder.Entity<Denuncia>(entity =>
            {
                entity.HasKey(e => new { e.IdDenuncia, e.IdDenunciante, e.IdDenunciado, e.Data });

                entity.Property(e => e.IdDenuncia)
                    .HasColumnName("ID_Denuncia")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.IdDenunciante).HasColumnName("ID_Denunciante");

                entity.Property(e => e.IdDenunciado).HasColumnName("ID_Denunciado");

                entity.Property(e => e.Data)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DataResposta)
                    .HasColumnName("Data_Resposta")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.IdAposta).HasColumnName("ID_Aposta");

                entity.Property(e => e.IdComentario).HasColumnName("ID_Comentario");

                entity.Property(e => e.Motivo)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RespostaAdmin)
                    .HasColumnName("Resposta_admin")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDenuncia).HasColumnName("Tipo_Denuncia");

                entity.HasOne(d => d.IdDenunciadoNavigation)
                    .WithMany(p => p.DenunciaIdDenunciadoNavigation)
                    .HasForeignKey(d => d.IdDenunciado)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Denuncia__ID_Den__25518C17");

                entity.HasOne(d => d.IdDenuncianteNavigation)
                    .WithMany(p => p.DenunciaIdDenuncianteNavigation)
                    .HasForeignKey(d => d.IdDenunciante)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Denuncia__ID_Den__245D67DE");
            });

            modelBuilder.Entity<Faqs>(entity =>
            {
                entity.HasKey(e => e.IdFaq);

                entity.ToTable("FAQs");

                entity.Property(e => e.IdFaq)
                    .HasColumnName("ID_FAQ")
                    .ValueGeneratedNever();

                entity.Property(e => e.Data).HasColumnType("date");

                entity.Property(e => e.DescriçaoFaq)
                    .IsRequired()
                    .HasColumnName("Descriçao_FAQ")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.TituloFaq)
                    .IsRequired()
                    .HasColumnName("Titulo_FAQ")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Liga>(entity =>
            {
                entity.HasKey(e => e.IdL);

                entity.Property(e => e.IdL).HasColumnName("ID_L");

                entity.Property(e => e.IdModalidade).HasColumnName("ID_Modalidade");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdModalidadeNavigation)
                    .WithMany(p => p.Liga)
                    .HasForeignKey(d => d.IdModalidade)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Liga__ID_Modalid__4222D4EF");
            });

            modelBuilder.Entity<LigaAposta>(entity =>
            {
                entity.HasKey(e => new { e.IdAposta, e.IdLiga });

                entity.Property(e => e.IdAposta).HasColumnName("ID_Aposta");

                entity.Property(e => e.IdLiga).HasColumnName("ID_Liga");

                entity.HasOne(d => d.IdApostaNavigation)
                    .WithMany(p => p.LigaAposta)
                    .HasForeignKey(d => d.IdAposta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LigaApost__ID_Ap__1CBC4616");

                entity.HasOne(d => d.IdLigaNavigation)
                    .WithMany(p => p.LigaAposta)
                    .HasForeignKey(d => d.IdLiga)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LigaApost__ID_Li__1DB06A4F");
            });

            modelBuilder.Entity<Modalidade>(entity =>
            {
                entity.HasKey(e => e.IdM);

                entity.Property(e => e.IdM).HasColumnName("ID_M");

                entity.Property(e => e.Foto)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SeguirUtilizador>(entity =>
            {
                entity.HasKey(e => new { e.IdUtilizadorSegue, e.IdUtilizadorSeguido });

                entity.Property(e => e.IdUtilizadorSegue).HasColumnName("ID_Utilizador_Segue");

                entity.Property(e => e.IdUtilizadorSeguido).HasColumnName("ID_Utilizador_Seguido");

                entity.HasOne(d => d.IdUtilizadorSegueNavigation)
                    .WithMany(p => p.SeguirUtilizadorIdUtilizadorSegueNavigation)
                    .HasForeignKey(d => d.IdUtilizadorSegue)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SeguirUti__ID_Ut__7C4F7684");

                entity.HasOne(d => d.IdUtilizadorSeguidoNavigation)
                    .WithMany(p => p.SeguirUtilizadorIdUtilizadorSeguidoNavigation)
                    .HasForeignKey(d => d.IdUtilizadorSeguido)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__SeguirUti__ID_Ut__7D439ABD");
            });

            modelBuilder.Entity<TipoAposta>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descricao)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Utilizador>(entity =>
            {
                entity.Property(e => e.DataNascimento)
                    .HasColumnName("Data_nascimento")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.DataRegisto)
                    .HasColumnName("Data_registo")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FotoPerfil)
                    .HasColumnName("Foto_perfil")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Genero)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdFacebook)
                    .HasColumnName("Id_Facebook")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdGoogle)
                    .HasColumnName("Id_Google")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Localidade)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModalidadeFavorita).HasColumnName("Modalidade_favorita");

                entity.Property(e => e.Nome)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Provider)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.WinRate)
                    .HasColumnName("Win_Rate")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.ModalidadeFavoritaNavigation)
                    .WithMany(p => p.Utilizador)
                    .HasForeignKey(d => d.ModalidadeFavorita)
                    .HasConstraintName("FK__Utilizado__Modal__571DF1D5");
            });
        }
    }
}
