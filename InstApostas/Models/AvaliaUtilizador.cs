﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class AvaliaUtilizador
    {
        public int IdUtilizadorAvaliador { get; set; }
        public int IdUtilizadorAvaliado { get; set; }
        public int Classificacao { get; set; }

        public Utilizador IdUtilizadorAvaliadoNavigation { get; set; }
        public Utilizador IdUtilizadorAvaliadorNavigation { get; set; }
    }
}
