﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Administrador
    {
        public int IdAdmin { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
