﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Denuncia
    {
        public int IdDenuncia { get; set; }
        public int IdDenunciante { get; set; }
        public int? IdComentario { get; set; }
        public int? IdAposta { get; set; }
        public int IdDenunciado { get; set; }
        public int TipoDenuncia { get; set; }
        public DateTime Data { get; set; }
        public string Motivo { get; set; }
        public string RespostaAdmin { get; set; }
        public DateTime? DataResposta { get; set; }
        public bool Estado { get; set; }

        public Utilizador IdDenunciadoNavigation { get; set; }
        public Utilizador IdDenuncianteNavigation { get; set; }
    }
}
