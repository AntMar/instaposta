﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class SeguirUtilizador
    {
        public int IdUtilizadorSegue { get; set; }
        public int IdUtilizadorSeguido { get; set; }

        public Utilizador IdUtilizadorSegueNavigation { get; set; }
        public Utilizador IdUtilizadorSeguidoNavigation { get; set; }
    }
}
