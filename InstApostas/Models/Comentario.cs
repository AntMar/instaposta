﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Comentario
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public int IdUtilizador { get; set; }
        public int IdAposta { get; set; }
        public DateTime Data { get; set; }

        public Aposta IdApostaNavigation { get; set; }
        public Utilizador IdUtilizadorNavigation { get; set; }
    }
}
