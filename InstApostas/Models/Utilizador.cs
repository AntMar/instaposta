﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Utilizador
    {
        public Utilizador()
        {
            Aposta = new HashSet<Aposta>();
            AvaliaUtilizadorIdUtilizadorAvaliadoNavigation = new HashSet<AvaliaUtilizador>();
            AvaliaUtilizadorIdUtilizadorAvaliadorNavigation = new HashSet<AvaliaUtilizador>();
            Avaliacao = new HashSet<Avaliacao>();
            Comentario = new HashSet<Comentario>();
            DenunciaIdDenunciadoNavigation = new HashSet<Denuncia>();
            DenunciaIdDenuncianteNavigation = new HashSet<Denuncia>();
            SeguirUtilizadorIdUtilizadorSegueNavigation = new HashSet<SeguirUtilizador>();
            SeguirUtilizadorIdUtilizadorSeguidoNavigation = new HashSet<SeguirUtilizador>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime? DataNascimento { get; set; }
        public string Nome { get; set; }
        public string Provider { get; set; }
        public string FotoPerfil { get; set; }
        public string Genero { get; set; }
        public int? ModalidadeFavorita { get; set; }
        public int? Estado { get; set; }
        public string IdFacebook { get; set; }
        public string IdGoogle { get; set; }
        public DateTime? DataRegisto { get; set; }
        public double? WinRate { get; set; }
        public double? Classificacao { get; set; }
        public string Localidade { get; set; }
        public Boolean IsLogged { get; set; }

        public Modalidade ModalidadeFavoritaNavigation { get; set; }
        public ICollection<Aposta> Aposta { get; set; }
        public ICollection<AvaliaUtilizador> AvaliaUtilizadorIdUtilizadorAvaliadoNavigation { get; set; }
        public ICollection<AvaliaUtilizador> AvaliaUtilizadorIdUtilizadorAvaliadorNavigation { get; set; }
        public ICollection<Avaliacao> Avaliacao { get; set; }
        public ICollection<Comentario> Comentario { get; set; }
        public ICollection<Denuncia> DenunciaIdDenunciadoNavigation { get; set; }
        public ICollection<Denuncia> DenunciaIdDenuncianteNavigation { get; set; }
        public ICollection<SeguirUtilizador> SeguirUtilizadorIdUtilizadorSegueNavigation { get; set; }
        public ICollection<SeguirUtilizador> SeguirUtilizadorIdUtilizadorSeguidoNavigation { get; set; }
    }
}
