﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class Aposta
    {
        public Aposta()
        {
            Avaliacao = new HashSet<Avaliacao>();
            Comentario = new HashSet<Comentario>();
            LigaAposta = new HashSet<LigaAposta>();
        }

        public int Id { get; set; }
        public string Descricao { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        public double GrauConfianca { get; set; }
        public int IdUtilizadorRegista { get; set; }
        public double OddTotal { get; set; }
        public int IdTipoAposta { get; set; }
        public string Foto { get; set; }
        public bool? Ganha { get; set; }

        public TipoAposta IdTipoApostaNavigation { get; set; }
        public Utilizador IdUtilizadorRegistaNavigation { get; set; }
        public ICollection<Avaliacao> Avaliacao { get; set; }
        public ICollection<Comentario> Comentario { get; set; }
        public ICollection<LigaAposta> LigaAposta { get; set; }
    }
}
