﻿using System;
using System.Collections.Generic;

namespace InstApostas.Models
{
    public partial class LigaAposta
    {
        public int IdAposta { get; set; }
        public int IdLiga { get; set; }

        public Aposta IdApostaNavigation { get; set; }
        public Liga IdLigaNavigation { get; set; }
    }
}
